type gamestate = {
    players : player list ; (* the list order gives the turn order *)
    chance : card list ; (* list of chance cards *)
    cc : card list ; (* list of community chest cards *)
    spaces : space list (* order of spaces represents order on board *)
  }

and player =
  | Human of playerdata
  | Ai of playerdata

and playerdata = {
    money : int ;
    location : int ;
    props : string list ;
    name : string ;
    jailcard : bool
  }

and space =
  | Property of prop_data
  | Go
  | Jail of (string * int) list (* list records # of turns player has been
                                   stuck in jail *)
  | Gotojail
  | Freeparking
  | Chance
  | Communitychest
  | Utility of prop_data
  | Railroad of prop_data
  | Luxurytax of int
  | Incometax of int

and prop_data = {
    title : string ;
    purchaseprice : int ;
    houseprice : int ;
    rents : int list ; (* records rents corresponding to # of houses *)
    status : propstatus ;
    color : string }

and propstatus =
  | Unowned
  | Owned of string (* string = name of player who owns it*)
  | Monopoly of string (* string owns all other properties of this color *)
  | Houses of string * int (* int between 1 and 4 is the number of houses *)
  | Hotel of string
  | Mortgaged of string
  | Railroads of string * int (* int between 1 and 4 is the # of RR's owned *)
  | Utilities of string * int (* int between 1 and 2 is # of Utilities owned *)

and card =
  | Move of int * string (* int represents location the card moves you
                          to, with 0 = Go and 39 = Boardwalk *)
  | Pay of int * string (* string represents card text *)
  | Payall of int * string (* pay each other player int dollars *)
  | Collectfromall of int * string (* each other player pays you int dollars *)
  | Gotojailcard of string
  | Getoutofjail of string
  | Nearestutility of string
  | Nearestrailroad of string
  | Goback3spaces of string
  | Payrepairs of int * int * string (* (x,y) means pay x per house and y per hotel *)

and trade = {
    offer : trade_bundle ;
    request : trade_bundle
  }

and trade_bundle = {
    tplayer : player ;
    tmoney : int ;
    tproperties : string list
  }

let find_space (s : string) (g : gamestate) : space option =
  match (List.filter
           (fun x ->
            match x with
            | Property d
            | Utility d
            | Railroad d -> String.lowercase d.title = String.lowercase s
            | _ -> false)
           g.spaces) with
  | [] -> None
  | [y] -> Some y
  | _ -> failwith "Two spaces have the same title."

let find_player (s : string) (g : gamestate) : player option =
  match List.filter
          (fun x ->
           match x with
             | Human p
             | Ai p -> String.lowercase p.name = String.lowercase s )
          g.players with
  | [] -> None
  | [y] -> Some y
  | _ -> failwith "Two players have the same name."

let get_propdata (s : space) : prop_data =
  match s with
    | Property d
    | Utility d
    | Railroad d -> d
    | _ -> failwith "Has no prop_data."

(* return the property whose title is [s] *)
let get_property (s : string) (g : gamestate) : space =
  match find_space s g with
    | Some p -> p
    | None   -> failwith "Invalid property"

let prop_location (s : string) (g : gamestate) : int =
  let rec helper (lst : space list) (count : int) =
    match lst with
      | [] -> failwith "The space does not exist in the gamestate."
      | h::t -> if (get_property s g) = h
                then count
                else helper t (count + 1) in
  helper g.spaces 0

(* provides a function to sort a space list based on location in g *)
let compare_props (g : gamestate) (s1 : string) (s2 : string) : int =
  let n1 = prop_location s1 g in
  let n2 = prop_location s2 g in
  if n1 < n2 then -1
  else if n1 > n2 then 1
  else 0


let print_property_info (s : string) (g : gamestate) : unit =
  match find_space s g with
    | Some Property d
    | Some Utility d
    | Some Railroad d ->
      (match d.status with
         | Unowned -> Printf.printf "%i - %s - %s - Unowned\n"
                                    (prop_location d.title g) d.color d.title
         | Owned _ -> Printf.printf "%i - %s - %s - Rent = $%i\n"
                                    (prop_location d.title g) d.color d.title (List.hd d.rents)
         | Monopoly _-> Printf.printf "%i - %s - %s -  Rent = $%i\n"
                                      (prop_location d.title g) d.color d.title  ((List.hd d.rents) * 2)
         | Houses (_,n) -> Printf.printf "%i - %s - %s - %i house(s) - Rent = $%i\n"
                                         (prop_location d.title g) d.color d.title  n (List.nth d.rents n)
         | Hotel _ ->  Printf.printf "%i - %s - %s - Hotel - Rent = $%i\n"
                                     (prop_location d.title g) d.color d.title  (List.nth d.rents 5)
         | Mortgaged _ ->  Printf.printf "%i - %s - %s - Mortgaged\n"
                                         (prop_location d.title g) d.color d.title
         | Railroads (_,n) -> Printf.printf "%i - %s - Railroad - Rent = $%i\n"
                                         (prop_location d.title g) d.title (List.nth d.rents (n-1))
         | Utilities (_,n) -> Printf.printf "%i - %s - Utility - Rent = $(%i x dice roll)\n"
                                       (prop_location d.title g) d.title (List.nth d.rents (n-1))  )
    | _ -> ()

(* returns the name of nth space in gamestate.spaces *)
let spacename (n : int) (g : gamestate) : string =
  match List.nth g.spaces n with
    | Property d
    | Utility d
    | Railroad d -> d.title
    | Go -> "Go"
    | Jail _ -> "Jail"
    | Gotojail -> "Go to Jail"
    | Freeparking -> "Free Parking"
    | Chance -> "Chance"
    | Communitychest -> "Community Chest"
    | Luxurytax _ -> "Luxury Tax"
    | Incometax _ -> "Income Tax"


let print_player_assets (p : player) (g : gamestate) : unit =
  match p with
    | Human d
    | Ai d ->
       Printf.printf "\n%s has $%i, is on space %i/%i (%s), and owns:\n"
                     d.name d.money d.location (List.length g.spaces - 1)
                     (spacename d.location g) ;
       List.iter (fun s -> print_property_info s g) (List.sort (compare_props g) d.props) ;
       if d.jailcard
       then Printf.printf "Get Out of Jail Free Card\n"
       else ()

let print_currentplayer_assets (g : gamestate) : unit =
  match g.players with
    | [] -> ()
    | p::_ -> print_player_assets p g

let print_all_assets (g : gamestate) : unit =
  List.iter (fun p -> print_player_assets p g) g.players



let update_property (s : space) (g : gamestate) : gamestate =
  let sdata = match s with
    | Property d
    | Utility d
    | Railroad d -> d
    | _ ->
       failwith
         "update_property only works on Properties, Utilities, and Railroads" in
  let rec helper (head: space list) (tail : space list) : space list =
    match tail with
      | [] -> head
      | h::t ->
         match h with
           | Property x
           | Utility x
           | Railroad x ->
              if x.title = sdata.title
              then head @ [s] @ t
              else helper (head @ [h]) t
           | _ -> helper (head @ [h]) t in
  let newspaces = helper [] g.spaces in
  {g with spaces = newspaces}

let update_jail (newlist : (string * int) list) (g : gamestate) : gamestate =
  let rec helper (head: space list) (tail : space list) : space list =
    match tail with
      | [] -> head
      | h::t ->
         match h with
           | Jail _ ->  head @ [Jail newlist] @ t
           | _ -> helper (head @ [h]) t in
  let newspaces = helper [] g.spaces in
  {g with spaces = newspaces}



let update_player (p : player) (g : gamestate) : gamestate =
  let pdata = match p with
      | Human d
      | Ai d -> d in
  let rec helper (head: player list) (tail : player list) : player list =
    match tail with
      | [] -> head
      | h::t ->
         match h with
           | Human x
           | Ai x ->
              if x.name = pdata.name
              then head @ [p] @ t
              else helper (head @ [h]) t in
  let newplayers = helper [] g.players in
  {g with players = newplayers}



let propvalue (s : string) (g : gamestate): int =
           match find_space s g with
             | Some Property propdata
             | Some Utility propdata
             | Some Railroad propdata ->
               ( match propdata.status with
                  | Owned _
                  | Monopoly _
                  | Railroads _
                  | Utilities _ -> (propdata.purchaseprice / 2)
                  | Houses (_,n) ->
                     (propdata.purchaseprice / 2) +
                       (n * (propdata.houseprice / 2) )
                  | Hotel _ ->
                     (propdata.purchaseprice / 2) +
                       (5 * (propdata.houseprice / 2) )
                  | _ -> 0 )
             | _ -> 0


(** returns the sum of the players money together with the value of
    selling all houses and mortgaging all properties *)
let check_liquidity (s : string) (g : gamestate) : int =
   match find_player s g with
      | None -> 0
      | Some Human p
      | Some Ai p ->
         List.fold_right (fun x y -> x + y)
         (List.map (fun s' -> propvalue s' g) p.props) p.money





(******************************************************************************)
(***************************** INITIAL GAME DATA ******************************)
(******************************************************************************)


let initial_state : gamestate =  {
    players = [] ;
    chance = [
        Move (0,"Advance to Go (Collect $200).") ;
        Move (5, "Take a trip to Reading Railroad - \
                  If you pass Go, collect $200.") ;
        Move (11,"Advance to St. Charles Place - \
                  If you pass Go, collect $200.") ;
        Move (24,"Advance to Illinois Ave. - \
                  If you pass Go, collect $200.") ;
        Move (39,"Take a walk on the Boardwalk - \
                  Advance token to Boardwalk.") ;
        Pay (-50,"Bank pays you dividend of $50.") ;
        Pay (15,"Pay poor tax of $15.") ;
        Pay (-150,"Your building loan matures - \
                   Collect $150.") ;
        Pay(-100,"You have won a crossword competition - \
                  Collect $100.") ;
        Payall(50,"You have been elected Chairman of the Board - \
                   Pay each player $50.");
        Nearestutility "Advance token to nearest Utility. \
                        If unowned, you may buy it from the Bank. \
                        If owned, throw dice and pay owner a total \
                        ten times the amount thrown." ;
        Nearestrailroad "Advance token to the nearest Railroad \
                         and pay owner twice the rental to which \
                         he/she is otherwise entitled. If Railroad \
                         is unowned, you may buy it from the Bank." ;
        Getoutofjail "Get out of Jail Free - \
                      This card may be kept until needed, or traded/sold." ;
        Gotojailcard "Go directly to Jail - \
                  Do not pass Go, do not collect $200.";
        Goback3spaces "Go back 3 spaces." ;
        Payrepairs (25,100,"Make general repairs on your property - \
                            For each house, pay $25, for each hotel pay $100.")
      ] ;
    cc = [
      Move (0,"Advance to Go (Collect $200).") ;
      Pay (-200,"Bank error in your favor - Collect $200.");
      Pay (50,"Doctor's fees - Pay $50.") ;
      Pay (-50,"From sale of stock you get $50.");
      Pay (-100,"Holiday Fund matures - Receive $100.") ;
      Pay (-20,"Income tax refund - Collect $20.") ;
      Pay (-100,"Life insurance matures - Collect $100.") ;
      Pay (100, "Pay hospital fees of $100.") ;
      Pay (150, "Pay school fees of $150.") ;
      Pay (-25, "Receive $25 consultancy fee.") ;
      Pay (-10, "You have won second prize in a beauty contest - Collect $10") ;
      Pay (-100, "You inherit $100.") ;
      Collectfromall(50,"Grand Opera Night - \
                         Collect $50 from each player for opening night seats.") ;
      Collectfromall(10,"It is your birthday - \
                         Collect $10 from each player.") ;
      Getoutofjail "Get out of Jail Free - \
                    This card may be kept until needed, or traded/sold." ;
      Gotojailcard "Go directly to Jail - \
                Do not pass Go, do not collect $200.";
      Payrepairs(40,115,"You are assessed for street repairs - \
                         $40 per house - \
                         $115 per hotel.")
          ] ;
    spaces = [
        Go ;
        Property {
            title = "Mediterranean Avenue" ;
            purchaseprice = 60 ;
            houseprice = 50 ;
            rents = [2;10;30;90;160;250] ;
            status = Unowned ;
            color = "Brown"
          } ;
        Communitychest;
        Property {
            title = "Baltic Avenue" ;
            purchaseprice = 60 ;
            houseprice = 50 ;
            rents = [4;20;60;180;320;450] ;
            status = Unowned ;
            color = "Brown"
          } ;
        Incometax 200 ;
        Railroad {
            title = "Reading Railroad" ;
            purchaseprice = 200 ;
            houseprice = -1 ;
            rents = [25;50;100;200] ;
            status = Unowned ;
            color = "Railroad"
          } ;
        Property {
            title = "Oriental Avenue" ;
            purchaseprice = 100 ;
            houseprice = 50 ;
            rents = [6;30;90;270;400;550] ;
            status = Unowned ;
            color = "Light Blue"
          } ;
        Chance ;
        Property {
            title = "Vermont Avenue" ;
            purchaseprice = 100 ;
            houseprice = 50 ;
            rents = [6;30;90;270;400;550] ;
            status = Unowned ;
            color = "Light Blue"
          } ;
        Property {
            title = "Connecticut Avenue" ;
            purchaseprice = 120 ;
            houseprice = 50 ;
            rents = [8;40;100;300;450;600] ;
            status = Unowned ;
            color = "Light Blue"
          } ;
        Jail [] ;
        Property {
            title = "St. Charles Place";
            purchaseprice = 140 ;
            houseprice = 100 ;
            rents = [10;50;150;450;625;750] ;
            status = Unowned ;
            color = "Purple"
          } ;
        Utility {
            title = "Electric Company";
            purchaseprice = 150 ;
            houseprice = -1 ;
            rents = [4;10] ; (* pay rent * # rolled on dice *)
            status = Unowned ;
            color = "Utility"
          } ;
        Property {
            title = "States Avenue";
            purchaseprice = 140 ;
            houseprice = 100 ;
            rents = [10;50;150;450;625;750] ;
            status = Unowned ;
            color = "Purple"
          } ;
        Property {
            title = "Virginia Avenue";
            purchaseprice = 160 ;
            houseprice = 100 ;
            rents = [12;60;180;500;700;900] ;
            status = Unowned ;
            color = "Purple"
          } ;
        Railroad {
            title = "Pennsylvania Railroad" ;
            purchaseprice = 200 ;
            houseprice = -1 ;
            rents = [25;50;100;200] ;
            status = Unowned ;
            color = "Railroad"
          } ;
        Property {
            title = "St. James Place";
            purchaseprice = 180 ;
            houseprice = 100 ;
            rents = [14;70;200;550;750;950] ;
            status = Unowned ;
            color = "Orange"
          } ;
        Communitychest ;
        Property {
            title = "Tennessee Avenue";
            purchaseprice = 180 ;
            houseprice = 100 ;
            rents = [14;70;200;550;750;950] ;
            status = Unowned ;
            color = "Orange"
          } ;
        Property {
            title = "New York Avenue";
            purchaseprice = 200 ;
            houseprice = 100 ;
            rents = [16;80;220;600;800;1000] ;
            status = Unowned ;
            color = "Orange"
          } ;
        Freeparking ;
        Property {
            title = "Kentucky Avenue";
            purchaseprice = 220 ;
            houseprice = 150 ;
            rents = [18;90;250;700;875;1050] ;
            status = Unowned ;
            color = "Red"
          } ;
        Chance ;
        Property {
            title = "Indiana Avenue";
            purchaseprice = 220 ;
            houseprice = 150 ;
            rents = [18;90;250;700;875;1050] ;
            status = Unowned ;
            color = "Red"
          } ;
        Property {
            title = "Illinois Avenue";
            purchaseprice = 240 ;
            houseprice = 150 ;
            rents = [20;100;300;750;925;1100] ;
            status = Unowned ;
            color = "Red"
          } ;
        Railroad {
            title = "B. & O. Railroad";
            purchaseprice = 200 ;
            houseprice = -1 ;
            rents = [25;50;100;200] ;
            status = Unowned ;
            color = "Railroad"
          } ;
        Property {
            title = "Atlantic Avenue";
            purchaseprice = 260 ;
            houseprice = 150 ;
            rents = [22;110;330;800;975;1150] ;
            status = Unowned ;
            color = "Yellow"
          } ;
        Property {
            title = "Ventnor Avenue";
            purchaseprice = 260 ;
            houseprice = 150 ;
            rents = [22;110;330;800;975;1150] ;
            status = Unowned ;
            color = "Yellow"
          } ;
        Utility {
            title = "Water Works";
            purchaseprice = 150 ;
            houseprice = -1 ;
            rents = [4;10] ; (* pay rent * # rolled on dice *)
            status = Unowned ;
            color = "Utility"
          } ;
        Property {
            title = "Marvin Gardens";
            purchaseprice = 280 ;
            houseprice = 150 ;
            rents = [24;120;360;850;1025;1200] ;
            status = Unowned ;
            color = "Yellow"
          } ;
        Gotojail ;
        Property {
            title = "Pacific Avenue";
            purchaseprice = 300 ;
            houseprice = 200 ;
            rents = [26;130;390;900;1100;1275] ;
            status = Unowned ;
            color = "Green"
          } ;
        Property {
            title = "North Carolina Avenue";
            purchaseprice = 300 ;
            houseprice = 200 ;
            rents = [26;130;390;900;1100;1275] ;
            status = Unowned ;
            color = "Green"
          } ;
        Communitychest ;
        Property {
            title = "Pennsylvania Avenue";
            purchaseprice = 320 ;
            houseprice = 200 ;
            rents = [28;150;450;1000;1200;1400] ;
            status = Unowned ;
            color = "Green"
          } ;
        Railroad {
            title = "Short Line";
            purchaseprice = 200 ;
            houseprice = -1 ;
            rents = [25;50;100;200] ;
            status = Unowned ;
            color = "Railroad"
          } ;
        Chance ;
        Property {
            title = "Park Place";
            purchaseprice = 350 ;
            houseprice = 200 ;
            rents = [35;175;500;1100;1300;1500] ;
            status = Unowned ;
            color = "Dark Blue"
          } ;
        Luxurytax 100 ;
        Property {
            title = "Boardwalk";
            purchaseprice = 400 ;
            houseprice = 200 ;
            rents = [50;200;600;1400;1700;2000] ;
            status = Unowned ;
            color = "Dark Blue"
          } ;
      ]
  }
