open Gamestate

(** This module contains the functions used to query human players, parse
    their decisions, and update the gamestate accordingly. *)

(** Pre_roll begins a loop that gives the human player options before their
    roll. These include printing the gameboard, printing assets, offering
    trades, building houses, mortgaging properties, and rolling the die. *)
val pre_roll: gamestate -> gamestate

(** Post_roll begins a loop that gives the human player options after their
    roll. These include printing the gameboard, printing assets, offering
    trades, building houses, and mortgaging properties. If a player has
    negative money, they must sell off enough assets to return to positive
    before exiting the post_roll loop or they lose. *)
val post_roll: gamestate -> gamestate

(** Precondition: trade.request.owner is a human player.
    Respond_to_trade offers the human player the given trade and parses
    their response. The human player will have the option to print their
    assets and the current gamestate before deciding. After accepting or
    rejecting, the new gamestate will be produced. *)
val respond_to_trade: trade * gamestate -> bool
