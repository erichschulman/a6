open Gamestate
open Gameboard
open Turn_out
open Ai_turn


(*##########checking validity of inputs#######################################*)

(* given parsed player name, checks if valid and not equal to current player *)
let valid_player gs player =
  let player_list = List.tl gs.players in
  let rec helper player_list name =
    match player_list with
    | [] -> false
    | h::t -> let data = unwrap_player h in
              if ( (String.lowercase data.name) = (String.lowercase name) )
              then true
              else helper t name in
    helper player_list player

(*given a game state
  a parsed property name,
checks if it valid*)
let valid_property gs title =
  let props = gs.spaces in
  let rec helper = function
    |[] -> false
    |Property(d)::t ->
      if ( (String.lowercase d.title) = (String.lowercase title) )
      then true else helper t
    |Utility(d)::t ->
      if ( (String.lowercase d.title) = (String.lowercase title) )
      then true else helper t
    |Railroad(d)::t ->
      if ( (String.lowercase d.title) = (String.lowercase title) )
      then true else helper t
    | _::t -> helper t in
  helper props

(* Determines whether or not name is the owner of title. *)
let valid_owner gs name title =
  let player = unwrap_player (get_player name gs) in
  List.mem (String.lowercase title) (List.map String.lowercase player.props)

(*given amount of cash and player, returns
  true if player has more than this amount*)
let valid_amount (gs:gamestate) (name:string) (amount:int) =
  let player = unwrap_player (get_player name gs) in
  (player.money >= amount)

(*##########helpers for mortgage/unmortgage###################################*)

(* Given a gamestate and a list of property names, returns true if all
* properties in the list of property names have no houses on them. *)
let rec check_no_houses (gs: gamestate) (prop_names: string list): bool =
  match prop_names with
  | [] -> true
  | h::t -> let h_property_data = unwrap_property(get_property h gs) in
            match h_property_data.status with
            | Monopoly (_) -> true && (check_no_houses gs t)
            | Mortgaged (_) -> true && (check_no_houses gs t)
            | _ -> false

(* Given a gamestate, sees if current player can mortgage any properties. *)
let valid_mortgage (gs : gamestate) : bool =
  let data = unwrap_player (get_player (find_self gs) gs) in
  let rec helper = function
  |[] -> false
  |h::t -> let prop = unwrap_property (get_property h gs) in
            (match prop.status with
            | Unowned -> failwith "you own this"
            | Owned(_) -> true
            | Monopoly(owner)->
              let owner_data = unwrap_player(get_player owner gs) in
              let owner_props = owner_data.props in
              let owner_props_in_monopoly =
                List.filter
                  (fun x -> ((unwrap_property(get_property x gs)).color)
                            = prop.color) owner_props in
              (check_no_houses gs owner_props_in_monopoly) || (helper t)
            | Houses (_) -> helper t
            | Hotel (_) -> helper t
            | Mortgaged(_)-> helper t
            | Railroads(_) ->true
            | Utilities(_) -> true) in
  helper data.props

(* Given a gamestate, sees if current player can unmortgage any properties. *)
let valid_unmortgage (gs : gamestate) : bool =
  let data = unwrap_player (get_player (find_self gs) gs) in
  let rec helper = function
  |[] -> false
  |h::t -> let prop = unwrap_property (get_property h gs) in
            (match prop.status with
            | Unowned -> failwith "you own this"
            | Owned(_) -> helper t
            | Monopoly(_)-> helper t
            | Houses (_) ->helper t
            | Hotel (_) -> helper t
            | Mortgaged(_)-> true
            | Railroads(_) -> helper t
            | Utilities(_) -> helper t) in
  helper data.props


(* Given a gamestate, sees if current player can build any houses or hotels. *)
let valid_build gs =
  let data = unwrap_player (get_player (find_self gs) gs) in

  let rec helper = function
  |[] -> false
  |h::t -> let prop = unwrap_property (get_property h gs) in
            if( not (valid_amount gs data.name prop.houseprice) ) then helper t
            else
              (match prop.status with
              | Unowned -> failwith "you own this"
              | Owned(_) -> helper t
              | Monopoly(_)-> true
              | Houses (_) -> true
              | Hotel (_) -> helper t
              | Mortgaged(_)-> helper t
              | Railroads(_) -> helper t
              | Utilities(_) -> helper t) in
  helper data.props


(* Given a gamestate, sees if current player can sell any houses or hotels. *)
let valid_sell gs =
  let data = unwrap_player (get_player (find_self gs) gs) in
  let rec helper = function
  |[] -> false
  |h::t -> let prop = unwrap_property (get_property h gs) in
              (match prop.status with
              | Unowned -> failwith "you own this"
              | Owned(_) -> helper t
              | Monopoly(_)-> helper t
              | Houses (_) -> true
              | Hotel (_) -> true
              | Mortgaged(_)-> helper t
              | Railroads(_) -> helper t
              | Utilities(_) -> helper t) in
  helper data.props

(* Given a gamestate, determine if current player can buy the space he or
 * she is on. *)
let valid_buy gs =
  let data = unwrap_player (get_player (find_self gs) gs) in
  let space = List.nth gs.spaces data.location in
  match space with
  | Property(d) -> if (d.status = Unowned) &&
                        (valid_amount gs data.name d.purchaseprice)
                  then true
                  else false
  | Go -> false
  | Jail (_)-> false
  | Gotojail -> false
  | Freeparking -> false
  | Chance -> false
  | Communitychest -> false
  | Utility(d) -> if (d.status = Unowned) &&
                       (valid_amount gs data.name d.purchaseprice)
                  then true
                  else false
  | Railroad(d) -> if (d.status = Unowned)  &&
                        (valid_amount gs data.name d.purchaseprice)
                  then true
                  else false
  | Luxurytax(_) -> false
  | Incometax(_) -> false

(*############get input from player###########################################*)

(*
(*prompts human player to input list of property names,
rejects if invalid owner and prompts again

preconditon: player properties are not empty
precondition: valid palyer name*)
let rec choose_property (gs:gamestate) (name:string) =
  let () = print_endline "Please choose a valid property" in
  let title = Pervasives.read_line() in
  if (valid_owner gs name title) then title
  else choose_property gs name*)

(*prompts player yes or no, translates in true false,
if invalid answer prompts again*)
let rec accept_reject () : bool =
  match String.lowercase (read_line()) with
  | "y" -> true
  | "yes" -> true
  | "n" -> false
  | "no" -> false
  | _ ->
     Printf.printf
       "\nThat's not a valid response. Please type Y for yes or N for no.\n> " ;
         accept_reject ()


(******************************** TRADING *************************************)

(* Given a gamestate and the name of a property, return true if that property
 * can be traded. *)
let tradable  (g : gamestate) (title : string) : bool =
  (*let name = find_self g in*)
  let prop_data = unwrap_property (get_property title g) in
  match prop_data.status with
    | Unowned -> false
    | Owned(_) -> true
    | Monopoly(owner)-> let owner_data = unwrap_player(get_player owner g) in
                 let owner_props = owner_data.props in
                 let owner_props_in_monopoly =
                   List.filter (fun x ->
                                ((unwrap_property(get_property x g)).color)
                                = prop_data.color) owner_props in
                        check_no_houses g owner_props_in_monopoly
    | Houses (_) -> false
    | Hotel (_) -> false
    | Mortgaged(_)-> true
    | Railroads(_) -> true
    | Utilities(_) -> true



let print_current_trade (p1 : player) (p2 : player) (g : gamestate)
                        (offeredmoney : int) (requestedmoney : int)
                        (offeredprops : string list)
                        (requestedprops : string list)
                        (notfinalized : bool) : unit =
  let d1 = unwrap_player p1 in
  let d2 = unwrap_player p2 in
  let d1tradableprops = List.filter (tradable g) d1.props in
  let d2tradableprops = List.filter (tradable g) d2.props in
  (if notfinalized
  then
    (Printf.printf
      "\n%s owns the following properties which could be added to the trade:\n"
      d1.name ;
     List.iter (fun s -> Printf.printf "%i - %s - %s\n"
                                       (prop_location s g) s (get_color s g))
               (List.sort (compare_props g)
                          (List.filter (fun s -> not(List.mem s offeredprops))
                                       d1tradableprops)) ;
     Printf.printf
       "\n%s owns the following properties which could be added to the trade:\n"
       d2.name ;
     List.iter (fun s -> Printf.printf "%i - %s - %s\n"
                                       (prop_location s g) s (get_color s g))
               (List.sort (compare_props g)
                          (List.filter (fun s -> not(List.mem s requestedprops))
                                       d2tradableprops)) ;
     Printf.printf "\nThe current trade looks like:\n" )
  else Printf.printf
         "\nHi %s, the following trade has been offered to you:\n" d2.name ) ;

  Printf.printf "\n%s offers $%i\n" d1.name offeredmoney ;
  List.iter (fun s -> Printf.printf "%i - %s - %s\n"
                                    (prop_location s g) s (get_color s g))
            (List.sort (compare_props g) offeredprops) ;
  Printf.printf "\n%s offers $%i\n" d2.name requestedmoney ;
  List.iter (fun s -> Printf.printf "%i - %s - %s\n"
                                    (prop_location s g) s (get_color s g))
            (List.sort (compare_props g) requestedprops)

let respond_to_trade ((t, g) : trade * gamestate) : bool =
  let offer = t.offer in
  let request = t.request in
  print_current_trade offer.tplayer request.tplayer g offer.tmoney
                    request.tmoney offer.tproperties request.tproperties false;
  Printf.printf "\nDo you want to accept this trade? (Y/N)\n> " ;
  if accept_reject ()
  then true
  else false


let rec offer_helper (p1 : player) (g : gamestate) : int =
  let d1 = unwrap_player p1 in
  Printf.printf "\nHow much money would you like to offer, %s? (Max $%i)\n> "
                d1.name d1.money;
  try
    let n = int_of_string (read_line ()) in
    if 0 <= n && n <= d1.money
    then n
    else (Printf.printf "\nThat's not a valid amount. You have $%i.\n"
                        d1.money ;
          offer_helper p1 g)
  with _ -> (Printf.printf "\nThat's not a valid amount. You have $%i.\n"
                           d1.money ;  offer_helper p1 g)

let rec request_helper (p2 : player) (g : gamestate) : int =
  let d2 = unwrap_player p2 in
  Printf.printf
    "\nHow much money would you like to request from %s? (Max $%i)\n> "
    d2.name d2.money;
  try
    let n = int_of_string (read_line ()) in
    if 0 <= n && n <= d2.money
    then n
    else (Printf.printf "\nThat's not a valid amount. %s has $%i.\n"
                        d2.name d2.money ;
          offer_helper p2 g)
  with _ -> (Printf.printf "\nThat's not a valid amount. %s has $%i.\n"
                           d2.name d2.money ;
            offer_helper p2 g)



let rec build_trade (p1 : player) (p2 : player) (g : gamestate)
                (offeredmoney : int) (requestedmoney : int)
                (offeredprops : string list)
                (requestedprops : string list) : trade option =
  print_current_trade p1 p2 g offeredmoney requestedmoney
                      offeredprops requestedprops true ;
  Printf.printf
  "\nType N where N is an integer add or remove that property from the trade.\n";
  Printf.printf
  "Type OFFER to change the amount of money you are offering in the trade.\n";
  Printf.printf
 "Type REQUEST to change the amount of money you are requesting in the trade.\n";
  Printf.printf
  "Type DONE to offer this trade to %s.\n" (unwrap_player p2).name ;
  Printf.printf
    "Type CANCEL to cancel this trade.\n> " ;
  match String.uppercase (read_line ()) with
    | "CANCEL" -> None
    | "DONE" -> Some {offer = {tplayer = p1;
                               tmoney = offeredmoney;
                               tproperties = offeredprops} ;
                      request =
                        {tplayer = p2;
                         tmoney = requestedmoney;
                         tproperties = requestedprops}}
    | "OFFER" ->
       build_trade p1 p2 g (offer_helper p1 g)
                   requestedmoney offeredprops requestedprops
    | "REQUEST" ->
       build_trade p1 p2 g offeredmoney (request_helper p2 g)
                   offeredprops requestedprops
    | s -> trade_prop_helper s p1 p2 g offeredmoney requestedmoney
                             offeredprops requestedprops

and trade_prop_helper (s : string) (p1 : player) (p2 : player) (g : gamestate)
                (offeredmoney : int) (requestedmoney : int)
                (offeredprops : string list)
                (requestedprops : string list) : trade option =
  let d1 = unwrap_player p1 in
  let d2 = unwrap_player p2 in
  let d1tradableprops = List.filter (tradable g) d1.props in
  let d2tradableprops = List.filter (tradable g) d2.props in
  try
    let n = int_of_string s in
    let title = spacename n g in
    if List.exists (fun s -> s = title) offeredprops
    then build_trade p1 p2 g offeredmoney requestedmoney
                     (List.filter (fun s -> not(s = title)) offeredprops)
                     requestedprops
    else if List.exists (fun s -> s = title) requestedprops
    then build_trade p1 p2 g offeredmoney requestedmoney offeredprops
                     (List.filter (fun s -> not(s = title)) requestedprops)
    else if List.exists (fun s -> s = title) d1tradableprops
    then build_trade p1 p2 g offeredmoney requestedmoney (title::offeredprops)
                     requestedprops
    else if List.exists (fun s -> s = title) d2tradableprops
    then build_trade p1 p2 g offeredmoney requestedmoney offeredprops
                     (title::requestedprops)
    else failwith "Not a valid input"
  with _ -> Printf.printf "\nThat's not a valid input.\n" ;
            build_trade p1 p2 g offeredmoney requestedmoney offeredprops
                        requestedprops

let print_other_players (g : gamestate) : unit =
  let rec helper (lst : player list) : unit =
    match lst with
      | [] -> ()
      | (Human d)::tail
      | (Ai d)::tail -> Printf.printf "Type %s to trade with %s.\n"
                                      (String.uppercase d.name) d.name ;
                        helper tail in
  helper (List.tl g.players) ;
  Printf.printf "Type CANCEL if you don't want to trade.\n> "

(*given a game state prompts human player to input player name
rejects if invalid, and prompts again*)
let rec choose_player (g : gamestate) : string option =
  let p = match List.hd g.players with
      | Human d
      | Ai d -> d in
  Printf.printf "\nHi %s, who would you like to trade with?\n" p.name ;
  print_other_players g ;
  match String.uppercase (read_line()) with
    | "CANCEL" -> None
    | s ->
       if valid_player g s
       then Some s
       else (Printf.printf "Sorry, that's not a valid input.\n" ;
       choose_player g)

(*prompts player for trade information,
then executes*)
let trade (g : gamestate) : gamestate =
  let p1 = List.hd g.players in
  match choose_player g with
    | None -> g
    | Some name2 ->
  let p2 = get_player name2 g in
  match build_trade p1 p2 g 0 0 [] [] with
    | None -> g
    | Some t ->
       match t.request.tplayer with
         | Human p2 -> if respond_to_trade (t,g)
                       then process_trade t g true
                       else (Printf.printf "\n%s rejected the trade.\n"
                                           p2.name; g)
         | Ai p2 -> if Ai_turn.respond_to_trade (t,g)
                    then process_trade t g true
                    else (Printf.printf "\n%s rejected the trade.\n"
                                        p2.name; g)

(*###############valid mortages/builds ect.##################################*)

let mortgagable (title : string) (gs : gamestate) : bool =
  let prop_data = unwrap_property (get_property title gs) in
  match prop_data.status with
  | Unowned -> failwith "you don't own this"
  | Owned(_) -> true
  | Monopoly(owner)->
    let owner_data = unwrap_player(get_player owner gs) in
    let owner_props = owner_data.props in
    let owner_props_in_monopoly =
      List.filter
        (fun x -> ((unwrap_property(get_property x gs)).color)
                  =prop_data.color) owner_props in
    check_no_houses gs owner_props_in_monopoly
  | Houses (_) -> false
  | Hotel (_) -> false
  | Mortgaged(_)-> false
  | Railroads(_) -> true
  | Utilities(_) -> true

let unmortgagable title gs =
  let name = find_self gs in
  let prop_data = unwrap_property (get_property title gs) in
  if not (valid_amount gs name (prop_data.purchaseprice / 2) ) then false
      else
  match prop_data.status with
  | Unowned -> failwith "you don't own this"
  | Owned(_) -> false
  | Monopoly(_)-> false
  | Houses (_) ->false
  | Hotel (_) -> false
  | Mortgaged(_)-> true
  | Railroads(_) -> false
  | Utilities(_) -> false

let buildable title gs =
  let name = find_self gs in
  let prop_data = unwrap_property (get_property title gs) in
  if( not (valid_amount gs name prop_data.houseprice) ) then false
  else
    match prop_data.status with
    | Unowned -> failwith "you own this"
    | Owned(_) -> false
    | Monopoly(_)-> true
    | Houses (_) -> true
    | Hotel (_) -> false
    | Mortgaged(_)-> false
    | Railroads(_) -> false
    | Utilities(_) -> false


let sellable title gs =
  let prop_data = unwrap_property (get_property title gs) in
    match prop_data.status with
    | Unowned -> failwith "you own this"
    | Owned(_) -> false
    | Monopoly(_)-> false
    | Houses (_) -> true
    | Hotel (_) -> true
    | Mortgaged(_)-> false
    | Railroads(_) -> false
    | Utilities(_) -> false



(*#########combine parse with execution#####################################*)



let choose_mortgage_property (g : gamestate) : string option =
  let p = unwrap_player(List.hd g.players) in
  let props : string list = List.sort (compare_props g) p.props in
  let mortgagable_props : string list =
    List.filter (fun s -> mortgagable s g) props in
  let rec helper (lst : string list) : unit =
    match lst with
      | [] -> Printf.printf
                "\nType the number of the property you want to mortgage or\n" ;
              Printf.printf
                "Type BACK if you don't want to mortgage anything.\n> "
      | head::tail -> let data = unwrap_property(get_property head g) in
                      Printf.printf "%i - %s can be mortgaged for $%i\n"
                         (prop_location head g) head (data.purchaseprice / 2) ;
                      helper tail in
  let rec parse_mortgage (g : gamestate) : string option =
     match String.uppercase (read_line ()) with
      | "BACK" -> None
      | s ->
         try
           let n : int = (int_of_string s) in
           let title : string = (unwrap_property(List.nth g.spaces n)).title in
           if mortgagable title g
           then Some title
           else failwith "Not a mortgagable property"
         with
           | _ ->
        Printf.printf
       "Please either enter the number of a valid property or type BACK.\n> " ;
              parse_mortgage g in
    helper mortgagable_props ;
    parse_mortgage g


let choose_unmortgage_property (g : gamestate) : string option =
  let p = unwrap_player(List.hd g.players) in
  let props : string list = List.sort (compare_props g) p.props in
  let unmortgagable_props : string list =
    List.filter (fun s -> unmortgagable s g) props in
  let rec helper (lst : string list) : unit =
    match lst with
      | [] ->
         Printf.printf
           "\nType the number of the property you want to unmortgage or\n" ;
         Printf.printf
           "Type BACK if you don't want to unmortgage anything.\n> "
      | head::tail -> let data = unwrap_property(get_property head g) in
                      Printf.printf "%i - %s can be unmortgaged for $%i\n"
                      (prop_location head g) head
                      ((data.purchaseprice / 2) + (data.purchaseprice / 10)) ;
                      helper tail in
  let rec parse_unmortgage (g : gamestate) : string option =
     match String.uppercase (read_line ()) with
      | "BACK" -> None
      | s ->
         try
           let n : int = (int_of_string s) in
           let title : string =
             (unwrap_property(List.nth g.spaces n)).title in
           if unmortgagable title g
           then Some title
           else failwith "Not an unmortgagable property"
         with
           | _ ->
              Printf.printf
       "Please either enter the number of a valid property or type BACK.\n> " ;
                  parse_unmortgage g in
    helper unmortgagable_props ;
    parse_unmortgage g

let choose_build_property (g : gamestate) : string option =
  let p = unwrap_player(List.hd g.players) in
  let props : string list = List.sort (compare_props g) p.props in
  let buildable_props : string list =
    List.filter (fun s -> buildable s g) props in
  let rec helper (lst : string list) : unit =
    match lst with
      | [] ->
         Printf.printf
    "\nType the number of the property you want to build a house/hotel on or\n";
         Printf.printf "Type BACK if you don't want to build anything.\n> "
      | head::tail ->
         let data = unwrap_property(get_property head g) in
         (match data.status with
          | Houses (s,4) ->
             Printf.printf "%i - %s currently has 4 houses and rent is $%i.\n"
                           (prop_location head g) head (List.nth data.rents 4);
             Printf.printf
               "    A hotel costs $%i to build and rent would become $%i.\n\n"
               data.houseprice (List.nth data.rents 5)
          | Houses (s,1) ->
             Printf.printf "%i - %s currently has 1 house and rent is $%i.\n"
                       (prop_location head g) head (List.nth data.rents 1) ;
             Printf.printf
          "    Another house costs $%i to build and rent would become $%i.\n\n"
                           data.houseprice (List.nth data.rents 2)
          | Houses (s,n) ->
             Printf.printf "%i - %s currently has %i houses and rent is $%i.\n"
                      (prop_location head g) head n (List.nth data.rents n) ;
             Printf.printf
         "    Another house costs $%i to build and rent would become $%i.\n\n"
                  data.houseprice (List.nth data.rents (n+1))
          | Monopoly s ->
             Printf.printf "%i - %s currently has 0 houses and rent is $%i.\n"
                  (prop_location head g) head ((List.nth data.rents 0) * 2) ;
             Printf.printf
              "    A house costs $%i to build and rent would become $%i.\n\n"
                 data.houseprice (List.nth data.rents 1)
          | _ -> failwith "Not a buildable property."  ) ;
         helper tail in
  let rec parse_build (g : gamestate) : string option =
     match String.uppercase (read_line ()) with
      | "BACK" -> None
      | s ->
         try
           let n : int = (int_of_string s) in
           let title : string = (unwrap_property(List.nth g.spaces n)).title in
           if buildable title g
           then Some title
           else failwith "Not a buildable property"
         with
           | _ -> Printf.printf
       "Please either enter the number of a valid property or type BACK.\n> " ;
                  parse_build g in
    helper buildable_props ;
    parse_build g

let choose_sell_property (g : gamestate) : string option =
  let p = unwrap_player(List.hd g.players) in
  let props : string list = List.sort (compare_props g) p.props in
  let sellable_props : string list =
    List.filter (fun s -> sellable s g) props in
  let rec helper (lst : string list) : unit =
    match lst with
      | [] -> Printf.printf
  "\nType the number of the property you want to sell a house/hotel from or\n" ;
              Printf.printf "Type BACK if you don't want to sell anything.\n> "
      | head::tail ->
         let data = unwrap_property(get_property head g) in
         (match data.status with
          | Hotel s ->
             Printf.printf "%i - %s currently has a hotel and rent is $%i.\n"
                           (prop_location head g) head (List.nth data.rents 5);
             Printf.printf
          "    Selling the hotel is worth $%i, but rent would become $%i.\n\n"
                           (data.houseprice / 2) (List.nth data.rents 4)
          | Houses (s,1) ->
             Printf.printf "%i - %s currently has 1 house and rent is $%i.\n"
                           (prop_location head g) head (List.nth data.rents 1) ;
             Printf.printf
           "    Selling the house is worth $%i, but rent would become $%i.\n\n"
                           (data.houseprice / 2) ((List.hd data.rents) * 2)
          | Houses (s,n) ->
             Printf.printf "%i - %s currently has %i houses and rent is $%i.\n"
                       (prop_location head g) head n (List.nth data.rents n) ;
             Printf.printf
            "    Selling a house is worth $%i, but rent would become $%i.\n\n"
            (data.houseprice / 2) (List.nth data.rents (n-1))
          | _ -> failwith "Not a sellable property."  ) ;
         helper tail in
  let rec parse_sell (g : gamestate) : string option =
     match String.uppercase (read_line ()) with
      | "BACK" -> None
      | s ->
         try
           let n : int = (int_of_string s) in
           let title : string = (unwrap_property(List.nth g.spaces n)).title in
           if sellable title g
           then Some title
           else failwith "Not a sellable property"
         with
           | _ -> Printf.printf
        "Please either enter the number of a valid property or type BACK.\n> " ;
                  parse_sell g in
    helper sellable_props ;
    parse_sell g

let mortgage (g : gamestate) : gamestate =
  Printf.printf "\nChoose a property to mortgage:\n" ;
  match choose_mortgage_property g with
    | None ->  g
    | Some s ->  process_mortgage (get_property s g, g) true

(* Aks the player if they want to unmortgage the property they've chose, and
 * mortgages the property if yes. *)
let unmortgage (g : gamestate) : gamestate =
  Printf.printf "\nChoose a property to unmortgage:\n" ;
  match choose_unmortgage_property g with
    | None -> g
    | Some s ->  process_unmortgage (get_property s g, g) true

(* Asks the player if they want to build on a building they've chosen, and
 * buys the building if yes. *)
let build (g : gamestate) : gamestate =
  Printf.printf "\nChoose a property to build a house or hotel on:\n" ;
  match choose_build_property g with
    | None ->  g
    | Some s -> process_build 1 (get_property s g, g) true

(* Asks the player if they want to sell a building they've chosen, and sells
 * the building if yes. *)
let sell (g : gamestate) : gamestate =
  Printf.printf "\nChoose a property to sell a house or hotel from:\n" ;
  match choose_sell_property g with
    | None -> g
    | Some s ->  process_bulldoze 1 (get_property s g, g) true

(* Asks the player if they want to buy the property they're currently on.
 * If yes, buy the property. *)
let buy (g : gamestate) : gamestate =
  let p : player = get_player (find_self g) g in
  let pd : playerdata = unwrap_player p in
  let s : space  = List.nth g.spaces pd.location in
  let sd : prop_data = unwrap_property s in
  Printf.printf "\nAre you sure you want to buy %s for $%i? (Y/N)\n> "
                sd.title sd.purchaseprice ;
  if accept_reject()
  then
    (Printf.printf "\n%s has bought %s!\n" pd.name sd.title ;
    process_prop_purchase p s g)
  else g

(*##############################Printing Helpers##############################*)

(* Prints the information associated with the property with name s. *)
let print_property_info (s : string) (g : gamestate) : unit =
    match find_space s g with
    | Some Property d
    | Some Utility d
    | Some Railroad d ->
      (match d.status with
         | Unowned ->
            Printf.printf "%s is currently unowned, and costs $%i to buy.\n"
                                   d.title d.purchaseprice
         | Owned s -> Printf.printf "%s is owned by %s. Rent is $%i.\n"
                                    d.title s (List.hd d.rents)
         | Monopoly s ->
            Printf.printf
              "%s is owned by %s who has the %s monopoly. Rent is $%i.\n"
              d.title s  d.color ((List.hd d.rents) * 2)
         | Houses (s,n) ->
            Printf.printf
              "%s is owned by %s who has built %i house(s). Rent is $%i.\n"
              d.title s n (List.nth d.rents n)
         | Hotel s ->
            Printf.printf
              "%s is owned by %s who has built a hotel. Rent is $%i.\n"
              d.title s (List.nth d.rents 5)
         | Mortgaged s ->
            Printf.printf "%s is owned by %s, but it has been mortgaged.\n"
                          d.title s
         | Railroads (s,n) ->
            Printf.printf
              "%s is owned by %s, who owns %i total railroad(s). Rent is $%i.\n"
              d.title s n (List.nth d.rents (n-1))
         | Utilities (s,n) ->
            Printf.printf "%s is owned by %s. Rent is $(%i x dice roll).\n"
                          d.title s (List.nth d.rents (n-1))  )
    | _ -> ()


let rec printfail () : unit =
  Printf.printf "\nInvalid input, please try again.\n"

(* Prints the options a player has before they've rolled. *)
let print_pre_options (g : gamestate) : unit =
  let player = List.hd g.players in
  let (p : playerdata) = match player with
      | Human d
      | Ai d -> d in

  (if is_in_jail player g
   then (let jail = get_jail g.spaces in
        let turns = List.fold_left
                (fun acc x -> if fst x = p.name then (snd x)::acc else acc)
                [] (get_jaildata jail) in
        Printf.printf
          "\nHi %s. You're currently stuck in Jail.\n" p.name ;
        Printf.printf "You have been here for %i turns and have $%i.\n"
                      (List.hd turns) p.money;
        Printf.printf
          "If you don't roll a double within your next %i attempt(s),\n"
         (3 - (List.hd turns)) ;
        Printf.printf "you will be fined $50 and released from jail.\n" ;
        if p.jailcard
        then Printf.printf "\nType CARD to use your get out of jail free card."
        else ();
        if p.money > 49
        then Printf.printf "\nType PAY to pay the $50 fee to be released."
        else () )
   else if p.location = 10
   then (Printf.printf
           "\nHowdy %s! You're currently visiting Jail (10/39) and have $%i.\n"
           p.name  p.money ;
         print_property_info (spacename p.location g) g)
   else (Printf.printf
           "\nHowdy %s! You're currently on %s (%i/%i) and have $%i.\n"
           p.name (spacename p.location g) p.location
           ((List.length g.spaces)-1) p.money ;
         print_property_info (spacename p.location g) g)
  ) ;
  print_newline ();
  if valid_buy g
  then Printf.printf "Type BUY to purchase %s.\n" (spacename p.location g)
  else () ;
  if valid_build g
  then Printf.printf "Type BUILD to build a house or hotel.\n"
  else () ;
  if valid_unmortgage g
  then Printf.printf "Type UNMORTGAGE to unmortgage one of your properties.\n"
  else () ;
  Printf.printf "Type TRADE to offer another player a trade.\n" ;
  if valid_mortgage g
  then Printf.printf "Type MORTGAGE to mortgage one of your properties.\n"
  else () ;
  if valid_sell g
  then Printf.printf
         "Type SELL to sell a house or hotel off of one of your properties.\n"
  else () ;
  Printf.printf "Type BOARD to view the game board.\n" ;
  Printf.printf "Type ASSETS to view all players' assets.\n" ;
  Printf.printf
    "Type N where N is an integer to get information about space N.\n" ;
  if ((check_liquidity p.name g) >= 0 && p.money >= 0)
  then (Printf.printf "Type ROLL to roll the dice and move.\n> ")
  else if (check_liquidity p.name g) >=0
  then Printf.printf
     "You must sell houses or mortgage properties until you are out of debt.\n> "
  else Printf.printf
         "You don't own enough to get out of debt. Type DONE to concede.\n> "


(* Prints the options a player has after they've rolled. *)
let print_post_options (g : gamestate) : unit =
  let player = List.hd g.players in
  let (p : playerdata) = match List.hd g.players with
      | Human d
      | Ai d -> d in

  (if is_in_jail player g
   then (let jail = get_jail g.spaces in
        let turns = List.fold_left
                (fun acc x -> if fst x = p.name then (snd x)::acc else acc)
                [] (get_jaildata jail) in
        Printf.printf
          "\nHi %s. You're currently stuck in Jail.\n" p.name ;
        Printf.printf "You have been here for %i turns and have $%i.\n"
                      (List.hd turns) p.money;
        Printf.printf
          "If you don't roll a double within your next %i attempt(s),\n"
         (3 - (List.hd turns)) ;
        Printf.printf "you will be fined $50 and released from jail.\n" ;
        if p.jailcard
        then Printf.printf "\nType CARD to use your get out of jail free card."
        else () )
   else if p.location = 10
   then (Printf.printf
           "\nHowdy %s! You're currently visiting Jail (10/39) and have $%i.\n"
           p.name  p.money ;
         print_property_info (spacename p.location g) g)
   else (Printf.printf
           "\nHowdy %s! You're currently on %s (%i/%i) and have $%i.\n"
           p.name (spacename p.location g) p.location
           ((List.length g.spaces)-1) p.money ;
         print_property_info (spacename p.location g) g)
  ) ;

  print_newline ();
  if valid_buy g
  then Printf.printf "Type BUY to purchase %s.\n" (spacename p.location g)
  else () ;
  if valid_build g
  then Printf.printf "Type BUILD to build a house or hotel.\n"
  else () ;
  if valid_unmortgage g
  then Printf.printf "Type UNMORTGAGE to unmortgage one of your properties.\n"
  else () ;
  Printf.printf "Type TRADE to offer another player a trade.\n" ;
  if valid_mortgage g
  then Printf.printf "Type MORTGAGE to mortgage one of your properties.\n"
  else () ;
  if valid_sell g
  then Printf.printf
         "Type SELL to sell a house or hotel off of one of your properties.\n"
  else () ;
  Printf.printf "Type BOARD to view the game board.\n" ;
  Printf.printf "Type ASSETS to view all players' assets.\n" ;
  Printf.printf
    "Type N where N is an integer to get information about space N.\n" ;
  if ((check_liquidity p.name g) >= 0 && p.money >= 0)
  then (Printf.printf "Type DONE to end your turn.\n> ")
  else if (check_liquidity p.name g) >=0
  then Printf.printf
     "You must sell houses or mortgage properties until you are out of debt.\n> "
  else Printf.printf
         "You don't own enough to get out of debt. Type DONE to concede.\n> "


(************************* MAIN FUNCTIONS **************************************)

(* keeps track of how many doubles in a row a player's rolled. *)
let tridoubles = ref 0

(*prompts player for pre-roll choice then executes. See .mli for more info. *)
let rec pre_roll (g : gamestate) : gamestate =
  let player = List.hd g.players in
  let pname = get_player_name player in
  if (check_liquidity pname g) < 0
  then g
  else (
  print_pre_options g ;
  match String.uppercase (read_line()) with
  | "PAY" -> if is_in_jail player g
             then let g' = release_from_jail player g in
                  let player' = get_player pname g' in
                  let player'' = update_money player' (-50) in
                  let g'' = update_player player'' g' in pre_roll g''
             else (printfail () ; pre_roll g)
  | "CARD" -> if is_in_jail player g && has_card player
                  then let g' = release_from_jail player g in
                  let player' =
                    match get_player pname g' with
                    | Human data -> Human { data with jailcard = false }
                    | Ai data ->    Ai { data with jailcard = false } in
                  let g'' = update_player player' g' in pre_roll g''
             else (printfail () ; pre_roll g)
  | "BUY" -> if valid_buy g
             then pre_roll (buy g)
             else (printfail () ; pre_roll g)
  | "BUILD" -> if valid_build g
               then pre_roll (build g)
               else (printfail () ; pre_roll g)
  | "UNMORTGAGE" -> if valid_unmortgage g
                    then pre_roll (unmortgage g)
                    else (printfail () ; pre_roll g)
  | "TRADE" -> pre_roll (trade g)
  | "MORTGAGE" -> if valid_mortgage g
                  then pre_roll (mortgage g)
                  else (printfail () ; pre_roll g)
  | "SELL" -> if valid_sell g
              then pre_roll (sell g)
              else (printfail () ; pre_roll g)
  | "BOARD" -> (printboard g ; pre_roll g)
  | "ASSETS" -> (print_all_assets g ; pre_roll g)

  | "ROLL" ->
              let (p : playerdata) = match player with
                | Human d
                | Ai d -> d in
              if  p.money >= 0
              then (
              let (r1,r2) = dice_roll () in
              let in_jail = is_in_jail player g in
              let realdoubles = r1 = r2 && not in_jail &&
              (not (is_going_to_jail (r1,r2) player g)) in

              let g' =
                if in_jail && (get_prisoner player g |> snd) = 2
                then
                  (if r1 = r2
                   then release_from_jail player g
                   else let newplayer = update_money player (-50) in
                        release_from_jail newplayer (update_player newplayer g))
                else g in


              let new_game = change_board realdoubles ((r1,r2),g') true in
              if realdoubles && !tridoubles = 2
              then
                (Printf.printf
                   "%s rolled three doubles in a row and has been sent to Jail!"
                   p.name;
                 gotojail_helper g)
              else if realdoubles && !tridoubles < 2
              then
                (incr tridoubles; pre_roll new_game)
              else new_game )
              else (printfail() ; g)
  | s -> try (Gameboard.print_space_data (int_of_string s) g ; pre_roll g) with
           | Failure "int_of_string"
           | Failure "nth" -> (printfail () ; pre_roll g)
           | _ -> (printfail () ; pre_roll g) )

(* See .mli file *)
let rec post_roll (g : gamestate) : gamestate =
  let player = List.hd g.players in
  let pname = get_player_name player in
  if (check_liquidity pname g) < 0
  then g
  else (
  print_post_options g ;
  match String.uppercase (read_line()) with
  | "PAY" -> if is_in_jail player g
             then let g' = release_from_jail player g in
                  let player' = get_player pname g' in
                  let player'' = update_money player' (-50) in
                  let g'' = update_player player'' g' in post_roll g''
             else (printfail () ; post_roll g)
  | "CARD" -> if is_in_jail player g && has_card player
              then let g' = release_from_jail player g in
                   let player' =
                     match get_player pname g' with
                     | Human data -> Human { data with jailcard = false }
                     | Ai data ->    Ai { data with jailcard = false } in
                   let g'' = update_player player' g' in
                   post_roll g''
              else (printfail () ; post_roll g)
  | "BUY" -> if valid_buy g
             then post_roll (buy g)
             else (printfail () ; post_roll g)
  | "BUILD" -> if valid_build g
               then post_roll (build g)
               else (printfail () ; post_roll g)
  | "UNMORTGAGE" -> if valid_unmortgage g
                    then post_roll (unmortgage g)
                    else (printfail () ; post_roll g)
  | "TRADE" -> post_roll (trade g)
  | "MORTGAGE" -> if valid_mortgage g
                  then post_roll (mortgage g)
                  else (printfail () ; post_roll g)
  | "SELL" -> if valid_sell g
              then post_roll (sell g)
              else (printfail () ; post_roll g)
  | "BOARD" -> (printboard g ; post_roll g)
  | "ASSETS" -> (print_all_assets g ; post_roll g)
  | "DONE" -> let (p : playerdata) = match List.hd g.players with
                | Human d
                | Ai d -> d in
              if p.money >= 0
              then (tridoubles:= 0; g)
              else (printfail () ; post_roll g)
  | s -> try (Gameboard.print_space_data (int_of_string s) g ; post_roll g) with
           | Failure "int_of_string"
           | Failure "nth" -> (printfail () ; post_roll g)
           | _ -> (printfail () ; post_roll g) )
