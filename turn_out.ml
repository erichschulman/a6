open Gamestate
open Gameboard

(*############################################################################*)
(*##########################Types#############################################*)
(*############################################################################*)

type die = int*int

(*############################################################################*)
(*##########################misc helpers######################################*)
(*############################################################################*)

(* get and normalize input from user *)
let getinput () = read_line() |> String.lowercase

(*printing helper*)
let pmessage m = Printf.printf "\n%s\n" m

(* format a string so it is properly capitalized
   e.g. [pformat "hElLo"] becomes "Hello" *)
let pformat s = s |> String.lowercase |> String.capitalize

(*remove elt from list*)
let remove_from_list elt lst =
  List.fold_left (fun acc x -> if elt = x then acc else acc@[x]) [] lst


(* helper to shuffle_list *)
let rec shuffle_list_helper l acc len =
  match l with
  | [] -> acc
  | a::b -> let n = Random.int len in
            let randelt = (List.nth l n) in
            let nacc = List.append acc [randelt] in
            let nl = remove_from_list randelt l in
            shuffle_list_helper nl nacc (len - 1)

(* randomly shuffle the elements of a list *)
let shuffle_list lst = shuffle_list_helper lst [] (List.length lst)

(*append l1 to l2 minus duplicates*)
let union l1 l2 =
  List.fold_left (fun acc x -> if List.mem x acc then acc else x::acc) l2 l1

(* turn a list of strings into a string separated by commas *)
let rec slist_to_string l =
   match l with
   | [] -> ""
   | z::[] -> z
   | a::b -> a ^ ", " ^ (slist_to_string b)



(*remove all elements in list2 that are equal to any element in list1*)
let rec remove_equals list1 list2 =
  match list1 with
  | [] -> []
  | z::[] -> if List.mem z list2 then remove_from_list z list2 else list2
  | a::b -> if List.mem a list2 then remove_equals b (remove_from_list a list2)
            else                     remove_equals b list2

(* given a game space, return true if it is a property *)
let is_property space = match space with
  | Property data
  | Utility data
  | Railroad data -> true
  | _ -> false



(*############################################################################*)
(*############################# Getters ######################################*)
(*############################################################################*)


(* return the player whose name is [pname] *)
let get_player pname gs = match find_player pname gs with
                      | Some p -> p
                      | None   -> failwith "Invalid player"

(* return the property whose title is [propname] *)
let get_property propname gs = match find_space propname gs with
                      | Some p -> p
                      | None   -> failwith "Invalid property"

(* return the name of player *)
 let get_player_name player = match player with
                              | Human p
                              | Ai p    -> p.name

(* return the location of player *)
 let get_player_location player = match player with
                              | Human p
                              | Ai p    -> p.location

(* return the funds of player *)
 let get_player_money player = match player with
                              | Human p
                              | Ai p    -> p.money

(* return the properties of player *)
 let get_player_props player = match player with
                              | Human p
                              | Ai p    -> p.props

(* return the name of the owner of [property] *)
let get_owner property : string =
  match property with
     | Property data
     | Utility data
     | Railroad data -> (match data.status with
         | Unowned -> "This property is not owned"
         | Owned pname -> pname
         | Monopoly pname -> pname
         | Houses (pname,h) -> pname
         | Hotel pname -> pname
         | Mortgaged pname -> pname
         | Railroads (pname, i) -> pname
         | Utilities (pname, i) -> pname)
      | _ -> failwith "Invalid property (get_owner)"

(* return the title name of [property] *)
let get_prop_title property : string =
   match property with
        | Property data
        | Utility data
        | Railroad data -> data.title
        | _ -> failwith "Not a property (get_prop_title)"

(* return purchase price of [property] *)
let get_prop_price property : int =
   match property with
        | Property data
        | Utility data
        | Railroad data -> data.purchaseprice
        | _ -> failwith "Not a property (get_prop_price)"

(* return status of [property] *)
let get_prop_status property : propstatus =
   match property with
        | Property data
        | Utility data
        | Railroad data -> data.status
        | _ -> failwith "Not a property (get_prop_status)"

(* given a property, return the color of its set *)
let get_prop_color property =
  match property with
  | Property data
  | Utility data
  | Railroad data -> data.color
  | _ -> failwith "Not a property (get_prop_color)"

let get_color (title : string) (g : gamestate) : string =
  get_prop_color (get_property title g)

(* given a color, return the set of properties that belong to it *)
let rec get_color_set color spaces =
   match spaces with
   | [] -> []
   | a::b -> if is_property a && get_prop_color a = color
             then a :: get_color_set color b
             else get_color_set color b

(*############################################################################*)
(*############################# Updaters #####################################*)
(*############################################################################*)

(* return boolean if current space is jail*)
let is_jail = function
  | Jail data -> true | _ -> false

(* return the jail structure*)
let rec get_jail = function
   | [] -> failwith "Error get_jail"
   | a::b -> if is_jail a then a else get_jail b

(*return the pair list of current players in jail*)
let get_jaildata = function
   | Jail j -> j | _ -> failwith "Error (get_jaildata)"

(* return true if [player] is currently in jail *)
let is_in_jail player gs =
   let jail = get_jail gs.spaces in
   let jlist = match jail with | Jail lst -> lst
                              | _ -> failwith "Error (is_in_jail)" in
   let in_jail = ref false in
   List.iter (fun x -> if get_player_name player = fst x
                then (in_jail := true) else ()) jlist;
   !in_jail

(* remove [player] from the jail structure in jail space in the gamestate *)
let release_from_jail player gs : gamestate =
   let jail = get_jail gs.spaces in
   let jaildata = get_jaildata jail in
   let jaildata' =
      List.filter (fun x -> get_player_name player <> fst x) jaildata in
   let gs' = update_jail jaildata' gs in
   pmessage ((get_player_name player) ^ " has been released from jail.");
   gs'

(* return true if the player has a get out of jail free card*)
let has_card = function
  | Human data
  | Ai data -> data.jailcard

(* return the pair (name,turns stuck in jail) of a player currently in jail*)
let get_prisoner player gs : string * int =
  let jail = get_jail gs.spaces in
  let jaildata = get_jaildata jail in
  let prisoner = List.filter (fun x -> get_player_name player = fst x) jaildata
  in List.hd prisoner

(* return true if [property] has an owner *)
let is_owned property = match property with
  | Property data
  | Utility data
  | Railroad data -> get_prop_status property <> Unowned
  | _ -> failwith "Not a property (is_owned)"

(* add a sum of money to a player's balance*)
let update_money player money : player =
        (match player with
        | Human p -> Human { p with money = p.money + money }

        | Ai p -> Ai { p with money = p.money + money })

(* update a list of properties in the gamestate *)
let update_properties spacelist gs =
   List.fold_right update_property spacelist gs


(* return [property] with its status changed to [nstatus] *)
let update_status property nstatus =
        match property with
        | Property data -> Property { data with status = nstatus }
        | Utility data  -> Utility  { data with status = nstatus }
        | Railroad data -> Railroad { data with status = nstatus }
        | _ -> failwith "Not a valid property"

(* given a set of properties, update their status *)
let rec update_set_status propset status =
  match propset with
  | [] -> []
  | a::b -> update_status a status :: update_set_status b status


(* helper for update_ownership; reassigns board ownership of
   a set of properties to the player identified by [pname] *)
let rec update_board_ownership gs (pname: string) (propname: string) =
    let prop = get_property propname gs in
    let propstatus = get_prop_status prop in
    let newstatus = match propstatus with
                    | Unowned -> Owned pname
                    | Mortgaged owner -> Mortgaged pname
                    | Owned owner -> Owned pname
                    | Railroads (owner,n) -> Railroads (pname,1)
                    | Utilities (owner,n) -> Utilities (pname,1)
                    | _ -> failwith "Error (update_board_ownership)" in
    let newprop = update_status prop newstatus
    in update_property newprop gs


(* update space ownership and player ownership to set [prp] as owned by
   player named [pname] and to remove it from their previous owner's
   property list, if there was a previous owner *)
let update_ownership gs pname prp =
  let prop = get_property prp gs in
  let gs' = update_board_ownership gs pname prp in

  let gs'' = if is_owned prop then
  let previous_owner = get_player ((get_property prp gs) |> get_owner) gs in
  let previous_owner' = match previous_owner with
            | Human p -> Human { p with props = remove_equals [prp] p.props}
            | Ai p    -> Ai    { p with props = remove_equals [prp] p.props}
  in update_player previous_owner' gs'

  else gs'
  in
  let newowner = get_player pname gs in
  let newowner' = match newowner with
                | Human p -> Human { p with props = union [prp] (p.props) }
                | Ai p    -> Ai    { p with props = union [prp] (p.props) }
  in

  update_player newowner' gs''

  (*Generalizes update_ownership for a list of property titles*)
  let update_ownerships gs name proplist : gamestate =
  List.fold_left (fun acc x -> update_ownership acc name x) gs proplist

(*############################################################################*)
(*######################## Monopoly status handling ##########################*)
(*############################################################################*)

(* return true if all the propreties in spacelist have monopoly status*)
let rec check_all_monops spacelist owner =
     match spacelist with
     | [] -> true
     | a::b -> get_prop_status a = Monopoly owner && check_all_monops b owner

(* return a list with all the mortgaged lists of a colorset *)
let rec get_mortgaged colorset owner =
        match colorset with
       | [] -> []
       | a::b -> if (get_prop_status a = Mortgaged owner)
                 then a :: (get_mortgaged b owner) else get_mortgaged b owner

(* return true if there are no mortgaged properties in the property list
  [cset], else false *)
  let rec check_no_mortgages cset =
   List.fold_left (fun acc x -> (match (get_prop_status x) with
                                 | Mortgaged owner -> (acc && false)
                                 | _ -> (acc && true))) true cset


(* helper for update_monopoly that checks a property set that is
  owned by a single player *)
let check_color_set owner color cset gs (print : bool) =
   let rec get_ownedprops colorset ownr: space list =
       match colorset with
       | [] -> []
       | a::b -> if (get_prop_status a = Owned ownr)
                 then a :: get_ownedprops b ownr
                 else get_ownedprops b ownr in
   let propsowned = get_ownedprops cset owner in
   let fstprop = if propsowned = [] then [] else [List.nth propsowned 0] in
   let restset = remove_equals fstprop cset in
   let rec check_own_or_monop colorset owner' =
       match colorset with
       | [] -> true
       | a::b -> (let statusa = get_prop_status a in
                      statusa = Owned owner' || statusa = Monopoly owner')
                      && check_own_or_monop b owner' in
   let result = if restset = []
                then []
                else
                if check_own_or_monop restset owner && check_no_mortgages cset
                then (
                  (if print
                  then pmessage
                  ("**" ^ owner ^ " has gained the " ^ color ^" monopoly!**")
                  else () ) ;
                 update_set_status cset (Monopoly owner) )
                else [] in
    let gs' = update_properties result gs in
    gs'

(* helper for update_monopoly that handles mortgages *)
let check_justmortgaged color (gs:gamestate) print =
  let cset = get_color_set color gs.spaces in
  let owner = get_owner (List.nth cset 0) in

  let mortgages = get_mortgaged cset owner in
  let nomorts = remove_equals mortgages cset in



  let do_downgrade =
     if (List.length mortgages = 1) && check_all_monops nomorts owner
     then (if print then
          pmessage ("**" ^ owner ^ " has lost the " ^ color ^ " monopoly.**"
           ) else ();
                      update_set_status nomorts (Owned owner) )
     else [] in
  let gs' = update_properties do_downgrade gs in
  gs'


(* helper for update_monopoly that handles utilities and railroads *)
let update_util_rr gs (colorset:space list) (owner:string) (prop:space) =
  let set = ref 0 in
  let ownerprops = get_player owner gs |> get_player_props in
  let setprops = List.fold_left (fun acc x ->
                 (get_prop_title x, get_prop_status x) :: acc) [] colorset in
  List.iter (fun x -> if List.mem (fst x) ownerprops &&
            (snd x <> Mortgaged owner) then incr set else ()) setprops;
  let updatedprop =
      if get_prop_status prop = Mortgaged owner then prop
      else
      match prop with
         | Railroad data -> update_status prop (Railroads (owner,!set))
         | Utility data -> update_status prop (Utilities (owner,!set))
         | _ -> failwith "Not a utility or railroad"   in
  update_property updatedprop gs

(* given a property title, check to see if it has become part
   of a monopoly, or if it is already in monopoly status,
   then check if the monopoly to which it belongs has been broken up.
   Then, update its status and that of the other members of
   its color set accordingly and return the new gamestate *)
let update_monopoly (gs:gamestate) (print : bool) (propname:string) =
  let prop = get_property propname gs in
  let color = get_prop_color prop in
  let cset = get_color_set color gs.spaces in
  let owner = get_owner prop in

  if color = "Utility" || color = "Railroad"
  then update_util_rr gs cset owner prop

  else

  let rec is_owned_by_same colset =
      match colset with
      | [] -> true
      | a::b -> get_owner a = owner && is_owned_by_same b in
  let owned_by_same = is_owned_by_same cset in
  let gs' = if owned_by_same then check_color_set owner color cset gs print
            else gs in

  let gs'' = if owned_by_same
             then check_justmortgaged color gs' print

             else let csetbool =
             List.fold_left (fun acc x -> if get_prop_status x = Monopoly owner
                    then (update_status x (Owned owner)::(fst acc),true) else
                          (x::(fst acc),snd acc)) ([],false) cset
             in let _ = if snd csetbool && owned_by_same && print then
                pmessage (color ^ " monopoly lost by " ^ owner ^ ".") else ()
             in let cset' = fst csetbool in
             update_properties cset' gs'
  in gs''

(* Generalizes update_monopoly for all the properties of a player *)
let update_player_monopolies gs (print :bool) player : gamestate =
  let props = get_player_props player in
  List.fold_left (fun g s -> update_monopoly g print s) gs props

(* Generalizes update_monopoly for all properties of all players *)
let update_game_monopolies (gs : gamestate) (print : bool) : gamestate =
  List.fold_left (fun g p -> update_player_monopolies g print p) gs gs.players


(*############################################################################*)
(*##########################misc helpers######################################*)
(*############################################################################*)

(*given a player, return data*)
let unwrap_player (player:player )= (match player with
  |Human(d) -> d
  |Ai(d) -> d)

(*given a property return underlying data*)
let unwrap_property (prop:space) =
  match prop with
  |Property(d) -> d
  |Utility(d) -> d
  |Railroad(d) -> d
  | _->failwith "invalid property"

(*given property title return
proprty object from gamestate*)
let find_property gs title =
  let props = gs.spaces in
  let rec helper = function
    |[] -> failwith "invalid title"
    |Property(d)::t -> if (d.title = title) then Property(d) else helper t
    |Utility(d)::t -> if (d.title = title) then Utility(d) else helper t
    |Railroad(d)::t ->if (d.title = title) then Railroad(d) else helper t
    | _::t -> helper t
    in helper props

(*returns name of current player
as string given game state*)
let find_self gs =
  match gs.players with
  | h::t -> (unwrap_player h).name
  | _ -> failwith "no player"


(*############################manipulating houses#############################*)

(* helper for process_build *)
let rec buildup i cost space =
  match i with
  | 0 -> (space,cost)
  | 1 ->  begin
     match space with
     | Property data ->
       (match data.status with
          | Monopoly owner ->
            (Property { data with status = Houses (owner,1) },
             cost + data.houseprice)
          | Houses (owner,nh) -> if nh < 4 then
            (Property { data with status = Houses (owner,nh+1) },
             cost + data.houseprice)
             else
            (Property { data with status = Hotel owner },
             cost + data.houseprice)

          | Hotel owner -> failwith "Cannot build more here"
          | _ ->           failwith "Cannot build here")

     | _ -> failwith "Not a valid property for that transaction"
     end

  | n ->
     begin
     match space with
     | Property data ->
       (match data.status with
          | Monopoly owner -> buildup (n-1) (cost + data.houseprice)
                           (Property { data with status = Houses (owner,1)    })
          | Houses (owner,nh) ->
                           if nh < 4 then buildup (n-1) (cost + data.houseprice)
                           (Property { data with status = Houses (owner,nh+1) })
                                     else buildup (n-1) (cost + data.houseprice)
                           (Property { data with status = Hotel owner })
          | Hotel owner -> failwith "Cannot build more here"
          | _ ->           failwith "Cannot build here")

     | _ -> failwith "Not a valid property for that transaction"
     end

(* helper for process_bulldoze *)
let rec bulldoze i money space =
  match i with
  | 0 -> (space,money)
  | n ->
     begin
     match space with
     | Property data ->
       (match data.status with
          | Hotel owner ->  bulldoze (n-1) (money + (data.houseprice / 2))
                           (Property { data with status = Houses (owner,4) })
          | Houses (owner,nh) ->
                  if nh > 1 then bulldoze (n-1) (money + (data.houseprice / 2))
                           (Property { data with status = Houses (owner,nh-1) })
                            else bulldoze (n-1) (money + (data.houseprice / 2))
                           (Property { data with status = Monopoly owner })
          | Monopoly owner -> failwith "Nothing more to bulldoze"
          | _ ->              failwith "Cannot bulldoze here")

     | _ -> failwith "Not a valid property for bulldoze"
     end


(*given an integer n of the number of houses (or hotel) to sell/tear down,
  a property space and the current gamestate, update the gamestate to
  reflect the changes in player balance and on the property status *)
let process_bulldoze (n:int) ((property:space),(gs:gamestate)) (print : bool) =
  let nspacemoney = bulldoze n 0 property in
  let newstate = update_property (fst nspacemoney) gs in
  let newplayer = match get_player (property |> get_owner) newstate with
                | Ai p    -> Ai    { p with money = p.money + (snd nspacemoney)}
                | Human p -> Human { p with money = p.money + (snd nspacemoney)}
  in
  let newstate' = update_player newplayer newstate in
  let d = unwrap_property property in
  (if print
   then (match d.status with
  | Hotel s -> Printf.printf
"\n%s sold the hotel on %s for $%i.\n4 houses are left and rent is now $%i.\n"
                            s d.title (d.houseprice / 2) (List.nth d.rents 4)
  | Houses (s,1) -> Printf.printf
"\n%s sold the house on %s for $%i.\nNo houses are left and rent is now $%i.\n"
                            s d.title (d.houseprice / 2) ((List.hd d.rents) * 2)
  | Houses (s,n) -> Printf.printf
"\n%s sold a house on %s for $%i.\n%i house(s) are left and rent is now $%i.\n"
                    s d.title (d.houseprice / 2) (n-1) (List.nth d.rents (n-1))
           | _ -> failwith "Not sellable")
    else ()) ;
  newstate'


(*############################################################################*)
(*##########################processing trades#################################*)
(*############################################################################*)

let print_accepted_trade (t : trade) (g : gamestate) : unit =
  let offer = t.offer in
  let request = t.request in
  let name1 = get_player_name (offer.tplayer) in
  let name2 = get_player_name (request.tplayer) in
  Printf.printf "\nTrade accepted!\n" ;
  Printf.printf "\n%s has traded $%i and the follwing properties to %s:\n"
                name1 offer.tmoney name2 ;
  List.iter (fun s -> Printf.printf "%i - %s - %s \n" (prop_location s g) s
            (get_color s g))
            (List.sort (compare_props g) offer.tproperties) ;
  Printf.printf "\n%s has traded $%i and the following properties to %s:\n"
                name2 request.tmoney name1;
  List.iter (fun s -> Printf.printf "%i - %s - %s\n" (prop_location s g) s
            (get_color s g))
            (List.sort (compare_props g) request.tproperties)

(* Outputs the gamestate obtained by doing the trade t.
 * if print = true, then the trade information will get printed *)
let process_trade (t : trade) (g : gamestate) (print : bool): gamestate =
  let offer = t.offer in
  let request = t.request in
  let p1 = offer.tplayer in
  let p2 = request.tplayer in
  (if print then print_accepted_trade t g else () ) ;
  let p1' = update_money p1 (request.tmoney - offer.tmoney) in
  let p2' = update_money p2 (offer.tmoney - request.tmoney) in
  let g1 = update_player p1' g  in
  let g2 = update_player p2' g1 in
  let g3 = update_ownerships g2 (get_player_name p1') request.tproperties in
  let g4 = update_ownerships g3 (get_player_name p2') offer.tproperties in
  update_game_monopolies g4 print

(*############################################################################*)
(*##########################processing mortgage###############################*)
(*############################################################################*)

(* given a call to process_mortgage or process_unmortgage, execute
   the appropriate changes *)
let do_mortgage_unmortgage v ((property:space),(gs:gamestate)):gamestate * int=
  let owner = get_owner property in
  let propprice = get_prop_price property in
  let money = propprice / 2 in
  let prop = if v = 0
      then update_status property (Mortgaged owner)
      else begin match property with
        | Property data -> Property { data with status = Owned owner }
        | Railroad data -> Railroad { data with status = Railroads (owner,1) }
        | Utility data ->  Utility  { data with status = Utilities (owner,1) }
        | _ -> failwith "Invalid prop (do_mortgage_unmortgage)"
        end in
  let newstate = update_property prop gs in
  (newstate, money)


(* return the gamestate reflecting the changes from prop being mortgaged *)
let process_mortgage ((prop:space),(gs:gamestate)) (print : bool) : gamestate =
   let gsfunds = do_mortgage_unmortgage 0 (prop,gs) in
   let newplayer = match get_player (prop |> get_owner) (fst gsfunds) with
                | Ai p    -> Ai    { p with money = p.money + snd gsfunds }
                | Human p -> Human { p with money = p.money + snd gsfunds } in
   let gs' = update_player newplayer (fst gsfunds) in
   let p = unwrap_player newplayer in
   let d = unwrap_property prop in
   (if print
   then Printf.printf "%s mortgaged %s for $%i.\n"
                      p.name d.title (d.purchaseprice / 2)
   else () );
   update_game_monopolies gs' print

(* return the gamestate reflecting the changes from prop being unmortgaged *)
let process_unmortgage ((prop:space),(gs:gamestate)) (print : bool) =
   let gsfunds = do_mortgage_unmortgage 1 (prop,gs) in
   let cost = (snd gsfunds) + ((snd gsfunds) / 5) in
   let newplayer = match get_player (prop |> get_owner) (fst gsfunds) with
                | Ai p    -> Ai    { p with money = p.money - cost }
                | Human p -> Human { p with money = p.money - cost } in
   let gs' = update_player newplayer (fst gsfunds) in
   let p = unwrap_player newplayer in
   let d = unwrap_property prop in
   (if print
   then Printf.printf "%s unmortgaged %s for $%i.\n"
            p.name d.title ((d.purchaseprice / 2) + (d.purchaseprice / 10))
   else () );
   update_game_monopolies gs' print

(*############################################################################*)
(*##########################processing building###############################*)
(*############################################################################*)

(*given an integer of the number of houses (or hotel) to build,
  a property space and the current gamestate, update the gamestate
  with the new status of the property and the resulting change
  in the owner's balance

pre: player can pay for the cost of the houses money*)
let process_build (n:int) ((property:space),(gs:gamestate)) print:gamestate =
  let nspacecost = buildup n 0 property in
  let newstate = update_property (fst nspacecost) gs in
  let newplayer = match get_player (property |> get_owner) newstate with
                | Ai p    -> Ai    { p with money = p.money - (snd nspacecost)}
                | Human p -> Human { p with money = p.money - (snd nspacecost)}
  in
  let newstate' = update_player newplayer newstate in
  let d = unwrap_property property in
  (if print
   then (match d.status with
         | Houses (s,4) ->
      Printf.printf "\n%s built a hotel on %s for $%i.\nIts new rent is $%i!\n"
                            s d.title d.houseprice (List.nth d.rents 5)
           | Houses (s,n) ->
Printf.printf "\n%s built another house on %s for $%i.\nIts new rent is $%i!\n"
                            s d.title d.houseprice (List.nth d.rents n)
           | Monopoly s ->
      Printf.printf "\n%s built a house on %s for $%i.\nIts new rent is $%i!\n"
                            s d.title d.houseprice (List.nth d.rents 1)
           | _ -> failwith "Not buildable")
   else ()) ;
   newstate'


(*############################################################################*)
(*###################processing purchase of properties########################*)
(*############################################################################*)

(* update the board when a property has been purchased by a player,
  add the property to the player and substract its cost from his funds *)
let process_prop_purchase (player: player) (space: space) gs: gamestate =
    let pname = get_player_name player in
    let color = get_prop_color space in
    let newspace =
    if      color = "Utility"  then update_status space (Utilities (pname,1))
    else if color = "Railroad" then update_status space (Railroads (pname,1))
    else                            update_status space (Owned pname) in

    let propcost = get_prop_price space in
    let newplayer =
        match player with
          | Ai p    -> Ai    { money = p.money - propcost;
                               location = p.location;
                               props = p.props@[get_prop_title space];
                               name = p.name;
                               jailcard = p.jailcard
                               }
          | Human p -> Human { money = p.money - propcost;
                               location = p.location;
                               props = p.props@[get_prop_title space];
                               name = p.name;
                               jailcard = p.jailcard
                               }
    in
    let new_state = update_player newplayer gs in
    let new_state' = update_property newspace new_state in
    update_game_monopolies new_state' true

(*############################################################################*)
(*##########################processing moves##################################*)
(*############################################################################*)

(*#########space helpers helper###############################################*)

(* Precondition: name must be a valid player name *)
let tax_helper (gs: gamestate) (tax: int) (name: string): gamestate =
  let player = unwrap_player(get_player name gs) in
  let player_type = match (get_player name gs) with
    | Human x -> true
    | Ai x -> false in
  let new_player_data = {location = player.location;
                         money = player.money - tax;
                         props = player.props;
                         name = player.name;
                         jailcard = player.jailcard} in
  let new_player =
    if player_type then Human (new_player_data)
    else Ai (new_player_data) in
  update_player new_player gs

let property_helper (gs: gamestate) (data: prop_data): gamestate =
  let current_player_name = find_self gs in
  let rent_list = data.rents in
  match data.status with
  | Unowned -> gs
  | Owned (owner) ->
    let amount_owed = List.nth rent_list 0 in
    let gs_player_paid = tax_helper gs amount_owed current_player_name in
    tax_helper gs_player_paid (-1*amount_owed) owner
  | Monopoly (owner) ->
    let amount_owed = (List.nth rent_list 0)*2 in
    let gs_player_paid = tax_helper gs amount_owed current_player_name in
    tax_helper gs_player_paid (-1*amount_owed) owner
  | Houses (owner, num_owned) ->
    let amount_owed = List.nth rent_list num_owned in
    let gs_player_paid = tax_helper gs amount_owed current_player_name in
    tax_helper gs_player_paid (-1*amount_owed) owner
  | Hotel (owner) ->
    let amount_owed = List.nth rent_list 5 in
    let gs_player_paid = tax_helper gs amount_owed current_player_name in
    tax_helper gs_player_paid (-1*amount_owed) owner
  | Mortgaged (owner) -> gs
  | _ -> failwith "This is a railroad or utility, not a property"

(*##########################going to jail and stuff###########################*)

let move_to_jail (gs: gamestate) (player_name: string) =
  let player = get_player player_name gs in
  let player_type =
    match player with
    | Human x -> true
    | Ai x -> false in
  let player_data =
    match player with
    | Human x -> x
    | Ai x -> x in
  let new_player_data = {player_data with location = 10} in
  let new_player = if player_type then Human (new_player_data)
  else Ai (new_player_data) in
  update_player new_player gs

let gotojail_helper (gs: gamestate): gamestate =
  let current_player = get_player (find_self gs) gs in
  let current_player_data = unwrap_player current_player in
  let current_jail_list =
    match (List.nth gs.spaces 10) with
    | Jail (pname_turn_lst) -> pname_turn_lst
    | _ -> failwith "Invalid jail space" in

  let gs_player_moved = move_to_jail gs current_player_data.name in
  let new_jail_list = (get_player_name current_player, 0)::current_jail_list in
  update_jail new_jail_list gs_player_moved


(*###########################chance and cc card###############################*)

let rec payall_helper gs player_lst current_player_name to_pay =
  match player_lst with
  | [] -> gs
  | h::t -> if (unwrap_player h).name <> current_player_name then
              let gs_player_paid = tax_helper gs to_pay current_player_name in
              let gs_player_received = tax_helper gs_player_paid (-1*to_pay)
               ((unwrap_player h).name) in
              payall_helper gs_player_received t current_player_name to_pay
            else payall_helper gs t current_player_name to_pay

(* Precondition: player_name is a valid name of a player in the game. *)
let move_spaces (gs: gamestate) (player_name: string) (move_to: int) (collect_on_go: bool) =
  let player = get_player player_name gs in
  let player_type =
    match player with
    | Human x -> true
    | Ai x -> false in
  let player_data =
    match player with
    | Human x -> x
    | Ai x -> x in
  let current_location = (unwrap_player player).location in
  let go_money = if (move_to < current_location) && collect_on_go then 200 else 0 in
  let new_player_data1 = {player_data with location = move_to} in
  let new_player_data =
    {new_player_data1 with money = new_player_data1.money + go_money} in
  let new_player = if player_type
  then Human (new_player_data) else Ai (new_player_data) in
  update_player new_player gs


(* Precondition: name must be a name of a player in the game. *)
let count_houses (gs: gamestate) (name: string): int =
  let players_data = unwrap_player(get_player name gs) in
  let properties = players_data.props in

  let rec helper (gs: gamestate) (prop_string_list: string list): int =
    match prop_string_list with
    | [] -> 0
    | h::t ->
      let current_property = get_property h gs in
      (match get_prop_status current_property with
      | Houses (_, i) -> i + (helper gs t)
      | _ -> 0 + (helper gs t)) in

  helper gs properties

let count_hotels (gs: gamestate) (name: string): int =
  let players_data = unwrap_player(get_player name gs) in
  let properties = players_data.props in

  let rec helper (gs: gamestate) (prop_string_list: string list): int =
    match prop_string_list with
    | [] -> 0
    | h::t ->
      let current_property = get_property h gs in
      (match get_prop_status current_property with
      | Hotel (_) -> 1 + (helper gs t)
      | _ -> 0 + (helper gs t)) in

  helper gs properties


let print_card_helper (c: card) : unit =
  match c with
  | Move (int_of_space, description) -> print_endline description
  | Pay (amount, description) -> print_endline description
  | Getoutofjail (description) -> print_endline description
  | Goback3spaces (description) -> print_endline description
  | Gotojailcard (description) -> print_endline description
  | Payrepairs (house_cost, hotel_cost, description) ->
                                              print_endline description
  | Payall (to_pay, description) -> print_endline description
  | Collectfromall (to_collect, description) -> print_endline description
  | Nearestutility (description) -> print_endline description
  | Nearestrailroad (description) -> print_endline description

let card_helper (gs: gamestate) (c: card): gamestate =
  let current_player = unwrap_player(List.hd (gs.players)) in
  let current_player_name = current_player.name in
  match c with
    | Move (int_of_space, description) ->
       move_spaces gs current_player_name int_of_space true
    | Pay (amount, description) -> tax_helper gs amount current_player_name
    | Getoutofjail (description) ->
      let current_player = List.hd gs.players in
      let current_player_type = match current_player with
        | Human x -> true
        | Ai x -> false in
      let current_player_data = match current_player with
        | Human x -> x
        | Ai x -> x in
      let updated_current_player_data =
        {current_player_data with jailcard = true} in
      let updated_current_player =
        if current_player_type then Human (updated_current_player_data)
        else Ai (updated_current_player_data) in
      update_player updated_current_player gs
    | Goback3spaces (description) ->
        move_spaces gs current_player_name (current_player.location - 3) false

    | Gotojailcard (description) -> gotojail_helper gs
    | Payrepairs (house_cost, hotel_cost, description) ->
      let current_player = unwrap_player (get_player (find_self gs) gs) in
      let num_houses = count_houses gs current_player.name in
      let num_hotels = count_hotels gs current_player.name in
      let amount_to_pay = house_cost*num_houses + hotel_cost*num_hotels in
      tax_helper gs amount_to_pay current_player.name
    | Payall (to_pay, description) -> payall_helper gs gs.players current_player_name to_pay
    | Collectfromall (to_collect, description) ->
        payall_helper gs gs.players current_player_name (-1*to_collect)

    | Nearestutility (description) ->
      let current_player = List.hd gs.players in
      let current_player_type = match current_player with
        | Human x -> true
        | Ai x -> false in
      let current_player_data = match current_player with
        | Human x -> x
        | Ai x -> x in
      let current_location = current_player_data.location in
      let new_location =
        if ((current_location <= 12) || (current_location > 29)) then 12 else 28 in
      let updated_current_player_data =
      {current_player_data with location = new_location} in
      let updated_current_player =
        if current_player_type then Human (updated_current_player_data)
        else Ai (updated_current_player_data) in
        update_player updated_current_player gs
    | Nearestrailroad (description) ->
      let current_player = List.hd gs.players in
      let current_player_type = match current_player with
        | Human x -> true
        | Ai x -> false in
      let current_player_data = match current_player with
        | Human x -> x
        | Ai x -> x in
      let current_location = current_player_data.location in
      let new_location =
        if current_location <= 5 || current_location > 35 then 5
        else if current_location > 5 || current_location <= 15 then 15
        else if current_location > 15 || current_location <= 25 then 25
        else 35 in
      let updated_current_player_data =
      {current_player_data with location = new_location} in
      let updated_current_player =
        if current_player_type then Human (updated_current_player_data)
        else Ai (updated_current_player_data) in
        update_player updated_current_player gs

let utility_helper (gs: gamestate) (data: prop_data) (d: die): gamestate =
  let current_player_name = find_self gs in
  match data.status with
    | Unowned -> gs
    | Mortgaged _ -> gs
    | Utilities (owner, num_owned) ->
      let amount_owed = if num_owned = 1 then 4*((fst d) + (snd d))
                        else 10*((fst d) + (snd d)) in
      let gs_player_paid = tax_helper gs amount_owed current_player_name in
      tax_helper gs_player_paid (-1*amount_owed) owner
    | _ -> failwith "invalid status for utility"

let railroad_helper (gs: gamestate) (data: prop_data): gamestate =
  let current_player_name = find_self gs in
  match data.status with
  | Unowned
  | Mortgaged _ -> gs
  | Railroads (owner, num_owned) ->
      let amount_owed = if num_owned = 1 then 25 else if num_owned = 2 then 50
                        else if num_owned = 3 then 100 else 200 in
      let gs_player_paid = tax_helper gs amount_owed current_player_name in
      tax_helper gs_player_paid (-1*amount_owed) owner
  | _ -> failwith "invalid status for railroad"


(*#############space helpers##################################################*)

let new_jail (d: die) (jail) (data: playerdata): (string*int) list =
  (* jail is a (string*int) list *)

  let rec new_jail_helper jail =
    (* Returns new jail list *)
    match jail with
    | [] -> []
    | h::t ->
      if fst h = data.name then
        if ((snd h) < 2) && (fst d) <> (snd d)
        then (fst h, (snd h) + 1)::(new_jail_helper t)
        else new_jail_helper t
      else
        h::(new_jail_helper t) in

  new_jail_helper jail


let count_spaces ((r1,r2): die) (jail) (gs: gamestate): int =
  (* if current player in jail, 0. Else, sum of die. *)
  (* jail is a (player*int) list *)

  let player = List.hd gs.players in
  let in_jail = is_in_jail player gs in

  if in_jail then
    (if r1 = r2
     then r1 + r2
     else 0 )
  else r1 + r2

(*given roll and game state, return new gamestate
where the player was moved, but the board wasn't updated
to reflect move

if the player passes go, he/she is given 200 dollars
if the player is in jail, then the jail is updated with an
extra turn spent in it by the player *)

let move_player ((d:die), (gs:gamestate)) (print : bool): gamestate*die =
  (*let player = List.hd gs.players *)

  let current_player = unwrap_player (get_player (find_self gs) gs) in
  let current_jail =
    match List.nth (gs.spaces) 10 with
    | Jail (jail_lst) -> jail_lst
    | _ -> failwith "11th space should be jail" in
  let spaces_moved = count_spaces d current_jail gs in
  let jail' = new_jail d current_jail current_player in
  let current_player_type = match (get_player (find_self gs) gs) with
    | Human x -> true
    | Ai x -> false in
  let go_bonus =
    if (current_player.location + spaces_moved = 40 && print)
  then (Printf.printf "\n%s landed on GO. Collect $200.\n" current_player.name;
          200)
    else if (current_player.location + spaces_moved > 40 && print)
    then (Printf.printf "\n%s passed GO. Collect $200.\n" current_player.name;
          200)
    else if current_player.location + spaces_moved >= 40
    then 200
    else 0 in
  let new_player_data =
    {location = ((current_player.location + spaces_moved) mod 40);
                    money = current_player.money + go_bonus;
                    props = current_player.props;
                    name = current_player.name;
                    jailcard = current_player.jailcard} in
  let new_player =
    if current_player_type then Human (new_player_data)
    else Ai(new_player_data) in
  (update_jail jail' (update_player new_player gs), d)

(*given new space update game state type game state to game state
pre-condition: player was moved, but the game state was not
updated to reflect new position*)

let dice_roll (): int*int =
  let () = Random.self_init () in
  ((Random.int 6) + 1,(Random.int 6) + 1)
   (* List.nth [(6,4);(5,5)] (Random.int 2) *)



let print_roll (d : die) (g : gamestate) (rdouble : bool) : unit =
  let (p : playerdata) = match List.hd g.players with
                | Human d
                | Ai d -> d in
  let (r1,r2) = d in
  Printf.printf "\n%s rolled a %i and a %i.\n" p.name r1 r2 ;
  if r1 = r2
  then
    (if rdouble
     then
     Printf.printf "Congratulations, it's a double! %s gets to roll again.\n"
                        p.name
     else if is_in_jail (List.hd g.players) g
     then ()
     else
     Printf.printf "Congratulations, %s rolled a double and is out of jail!\n"
                        p.name)
  else ()

let print_rent_payment ((r1,r2) : die) (g : gamestate): unit =
  let p = unwrap_player (get_player (find_self g) g) in
  let current_loc_int = p.location in
  let current_space = List.nth g.spaces current_loc_int in
  match current_space with
    | Property d
    | Utility d
    | Railroad d-> (match d.status with
        | Unowned -> Printf.printf "\n%s landed on %s which is unowned.\n"
                                               p.name d.title
        | Mortgaged s -> Printf.printf
        "\n%s landed on %s which is owned by %s, but mortgaged.\n"
                                                   p.name d.title s
        | Owned s -> if p.name = s
              then Printf.printf "\n%s landed on %s, which %s owns.\n"
                                                    p.name d.title p.name
              else Printf.printf "\n%s landed on %s and paid $%i rent to %s.\n"
                                          p.name d.title (List.hd d.rents) s
        | Monopoly s ->  if p.name = s
              then Printf.printf "\n%s landed on %s, which %s owns.\n"
                                                  p.name d.title p.name
              else Printf.printf "\n%s landed on %s and paid $%i rent to %s.\n"
                                        p.name d.title ((List.hd d.rents)*2) s
        | Houses (s,n) -> if p.name = s
                then Printf.printf "\n%s landed on %s, which %s owns.\n"
                                          p.name d.title p.name
              else Printf.printf "\n%s landed on %s and paid $%i rent to %s.\n"
                                        p.name d.title (List.nth d.rents n) s
        | Hotel s -> if p.name = s
                    then Printf.printf "\n%s landed on %s, which %s owns.\n"
                                                    p.name d.title p.name
              else Printf.printf "\n%s landed on %s and paid $%i rent to %s.\n"
                                        p.name d.title (List.nth d.rents 5) s
        | Railroads (s,n) -> if p.name = s
                    then Printf.printf "\n%s landed on %s, which %s owns.\n"
                                              p.name d.title p.name
              else Printf.printf "\n%s landed on %s and paid $%i rent to %s.\n"
                                    p.name d.title (List.nth d.rents (n-1)) s
          | Utilities (s,n) -> if p.name = s
                      then Printf.printf "\n%s landed on %s, which %s owns.\n"
                                                    p.name d.title p.name
              else Printf.printf "\n%s landed on %s and paid $%i rent to %s.\n"
                    p.name d.title ((List.nth d.rents (n-1)) * (r1 + r2)) s)
    | _ -> ()


let rec update_moved_player (rdoubles : bool) ((gs),(d: die), (post_card))=
  let p = get_player (find_self gs) gs in
  let current_player = unwrap_player p in
  let ishuman = match get_player (find_self gs) gs with
      | Human _ -> true
      | Ai _ -> false in
  let current_loc_int = current_player.location in
  let current_space = List.nth gs.spaces current_loc_int in
  match current_space with
  | Property prop_data -> let result = property_helper gs prop_data in
                          (if ishuman
                          then printboard result
                          else () );
                          print_roll d result rdoubles ;
                          print_rent_payment d result ;
                          result
  | Go ->  (if ishuman
            then printboard gs
            else () );
          gs
  | Jail (player_turn_lst) ->
     (if ishuman
      then printboard gs
      else () );
     print_roll d gs rdoubles;
     if is_in_jail p gs
     then let (s,n) = get_prisoner p gs in
          if n > 0
        then Printf.printf "\n%s is still stuck in Jail.\n" current_player.name
          else Printf.printf "\n%s has been thrown in Jail.\n" current_player.name
     else Printf.printf "\n%s landed on Jail.\n" current_player.name ;
     gs
  | Gotojail -> let result = gotojail_helper gs in
                (if ishuman
                 then printboard result
                 else () );
                print_roll d result rdoubles;
                Printf.printf
            "\n%s landed on Go To Jail. Go directly to jail, do not pass GO.\n"
                              current_player.name ;
                result
  | Freeparking -> (if ishuman
                    then printboard gs
                    else () );
                   print_roll d gs rdoubles ;
                   Printf.printf "\n%s landed on Free Parking.\n"
                                 current_player.name ;
                   gs
  | Chance ->
    if post_card then gs
    else
      let drawn_chance = List.hd gs.chance in
      let new_gs = card_helper gs drawn_chance in
      let current_chance_list = new_gs.chance in
      let new_chance_list =
      (List.tl current_chance_list)@[List.hd current_chance_list] in
      let new_gs' = {new_gs with chance = new_chance_list} in
      let result = update_moved_player rdoubles (new_gs', d, true) in
      (if ishuman
       then printboard result
       else () );
      print_roll d result rdoubles;
      Printf.printf "\n%s drew a chance card!\n" current_player.name ;
      print_card_helper drawn_chance ;
      result
  | Communitychest ->
    if post_card then gs
    else
      let drawn_cc = List.hd gs.cc in
      let new_gs = card_helper gs drawn_cc in
      let current_cc_list = new_gs.cc in
      let new_cc_list = (List.tl current_cc_list)@[List.hd current_cc_list] in
      let new_gs' = {new_gs with cc = new_cc_list} in
      let result = update_moved_player rdoubles (new_gs', d, true) in
      (if ishuman
       then printboard result
       else () );
      print_roll d result rdoubles;
      Printf.printf "\n%s drew a Community Chest card!\n" current_player.name ;
      print_card_helper drawn_cc ;
      result
  | Utility prop_data -> let result = utility_helper gs prop_data d in
                         (if ishuman
                          then printboard result
                          else () );
                         print_roll d result rdoubles;
                         print_rent_payment d result ;
                         result
  | Railroad prop_data -> let result = railroad_helper gs prop_data in
                          (if ishuman
                           then printboard result
                           else () );
                          print_roll d result rdoubles;
                          print_rent_payment d result ;
                          result
  | Luxurytax i ->
     let result = tax_helper gs i current_player.name in
     (if ishuman
      then printboard result
      else () );
     print_roll d result rdoubles;
     Printf.printf "\n%s landed on Luxury Tax and paid $%i.\n"
                   current_player.name i ;
     result
  | Incometax i ->
     let result = tax_helper gs i current_player.name in
     (if ishuman
      then printboard result
      else () );
     print_roll d result rdoubles;
     Printf.printf "\n%s landed on Income Tax and paid $%i.\n"
                   current_player.name i ;
     result

let change_board realdoubles ((d:die), (gs:gamestate)) (print : bool) =
  let (player_moved_gs, prev_die) = move_player (d,gs) print in
  update_moved_player realdoubles (player_moved_gs, prev_die, false)

(*############################################################################*)
(*##########################processing defeat#################################*)
(*############################################################################*)

(*if one player loses to another give his money to another player (so that he
doesn't double pay)

also need to decide where this is called*)


(* Given a player who has lost, reassign his assets according to
   the space in which they are when they lost.

   First, all his houses/hotels are sold.

   Then his funds and properties are assigned depending on where
   they landed when they lost (his current location)

   If they are currently located at a player's space,
   then they must have lost to them, so
   the owner of the space gets his properties and money.

   Otherwise, his properties are reset to unowned status *)
let process_defeat player gs : gamestate =

  let rec mass_bulldoze proplist player (gs': gamestate): gamestate =
   match proplist with
   | [] -> gs'
   | a::b -> let status_prop =
               match find_space a gs' with
               | Some prop -> (get_prop_status prop,prop)
               | None      -> failwith "Invalid property (mass_bulldoze)" in

             match fst status_prop with
             | Houses (s,i) -> mass_bulldoze b player
                                             (process_bulldoze i (snd status_prop,gs') true)

             | Hotel s ->      mass_bulldoze b player
                                             (process_bulldoze 5 (snd status_prop,gs') true)
             | _ -> mass_bulldoze b player gs
  in
  let gs'' = mass_bulldoze (get_player_props player) player gs in
  let player' = get_player (get_player_name player) gs'' in
  let loc = get_player_location player in
  let space_of_loss = List.nth gs.spaces loc in
  let defeater = match space_of_loss with
        | Property data
        | Utility data
        | Railroad data -> get_owner space_of_loss
        | _ -> "Bank"
   in
   let defeating_player = if defeater = "Bank"
                          then player'
                          else get_player defeater gs'' in
   let p1funds = get_player_money player' in
   let p1props = get_player_props player' in

   let offer'   = { tplayer = defeating_player;
                    tmoney = 0;
                    tproperties = [] }
   in
   let request' = { tplayer = player;
                    tmoney = p1funds;
                    tproperties = p1props }
   in
   let trade'  = { offer = offer'; request = request' } in

   let newgs = if defeater <> "Bank"
               then process_trade trade' gs'' true
               else
                let rec reset_props proplist gstate =
                   match proplist with
                   | [] -> gs
                   | a::b ->  pmessage (a ^ "has been reset to Unowned.");
                              reset_props b
                             (update_property (update_status
                               (get_property a gstate)
                               Unowned) gstate)
              in reset_props p1props gs''
   in update_game_monopolies newgs true

(*return true if [player] is heading to jail as a consequence of
the current dice roll*)
let is_going_to_jail dice player gamestate =
  let currentloc = get_player_location player in
  let futureloc = (fst dice + snd dice + currentloc) mod 40 in
  let futurespace = List.nth gamestate.spaces futureloc in
  let result = match futurespace with
               | Gotojail -> true
               | Chance  ->  (match List.hd gamestate.chance with
                                  | Gotojailcard d -> true | _ -> false)
               | Communitychest -> (match List.hd gamestate.cc with
                                  | Gotojailcard d -> true | _ -> false)
               | _ -> false
   in result
