open Gamestate
open Turn_out
open Gameboard

(*##############helpers for calculating loss on cards#########################*)

let chance_loss (gs: gamestate) (name: string): int =
  (* Right now this just averages the losses of each chance card.
     Very rough heuristic, can make more accurate later.

     We didn't end up having time to write this function. *)
  0


let cc_loss (gs: gamestate) (name: string): int =
  (* Right now this just averages the losses of each chance card.
     Very rough heuristic, can make more accurate later.

     We didn't end up having time to write this function. *)
  0


(*##############helpers for calculating loss/gain on properties###############*)

(* Figures out how much money a player would lose if they landed on the
   property with prop_data data *)
let property_loss (gs: gamestate) (name: string) (data:prop_data) : int =
  match data.status with
  | Unowned -> 0
  | Owned(owner) -> if(owner = name) then 0 else (List.nth data.rents 0 )
  | Monopoly(owner) -> if(owner = name) then 0 else 2*(List.nth data.rents 0 )
  | Houses(owner,num_houses) -> if(owner = name) then 0
                                else (List.nth data.rents num_houses)
  | Hotel(owner) -> if(owner = name) then 0 else (List.nth data.rents 5 )
  | Mortgaged(_)-> 0
  | Railroads(owner, num_rr)-> if(owner = name) then 0
                               else (List.nth data.rents (num_rr -1) )

  | Utilities(owner, num_util)-> if(owner = name) then 0
                                 else 7*(List.nth data.rents (num_util -1))

(*############################################################################*)

(* returns a postive number if you lose money on this space, else 0
Pre: location is space you moved i.e. could be greater than
40 *)
let lose_on_space (gs: gamestate) (loc: int) (name: string) : int =
  let go_bonus = (if (loc > 40) then -200 else 0) in
  match List.nth gs.spaces (loc mod 40) with
  | Property(p) -> (property_loss gs name p) + go_bonus
  | Go -> 0 + go_bonus
  | Jail(_) -> 0  + go_bonus(* list records # of turns player has been
                                   stuck in jail *)
  | Gotojail -> 0 + go_bonus
  | Freeparking -> 0 + go_bonus
  | Chance -> (chance_loss gs name) + go_bonus
  | Communitychest -> (cc_loss gs name) + go_bonus
                      (* Added up money from cards you lose on, divided by
                       * number of cards in the deck. *)
  | Utility(p) -> (property_loss gs name p) + go_bonus
  | Railroad(p) -> (property_loss gs name p) + go_bonus
  | Luxurytax i -> i + go_bonus
  | Incometax i -> i + go_bonus


(* Determines an estimate of what a player will lose on this turn. Weights how
   much the player would lose when landing on a space by the probability of
   landing on that space based on where they are currently. *)
let sum_penalties (gs:gamestate) (name:string) =
  let loc = (unwrap_player(get_player name gs)).location in
  let result =
  (1./.36.)*.((float_of_int (lose_on_space gs (loc+2) name) )+.
             (float_of_int (lose_on_space gs (loc+12) name) ))+.
  (2./.36.)*.((float_of_int (lose_on_space gs (loc+3) name) )+.
             (float_of_int (lose_on_space gs (loc+11) name) ))+.
  (3./.36.)*.((float_of_int (lose_on_space gs (loc+4) name) )+.
             (float_of_int (lose_on_space gs (loc+10) name) ))+.
  (4./.36.)*.((float_of_int (lose_on_space gs (loc+5) name) )+.
             (float_of_int (lose_on_space gs (loc+9) name) ))+.
  (5./.36.)*.((float_of_int (lose_on_space gs (loc+6) name) )+.
             (float_of_int (lose_on_space gs (loc+8) name) ))+.
  (6./.36.)*.(float_of_int (lose_on_space gs (loc+7) name) ) in
  (int_of_float result)

(*calculates an estimate of how much the player is going to lose in the future
 * by averaging losses across future gamestates*)
let rec expected_loss_base (gs:gamestate) (name:string) (i:int): int =
  if( i <= 1 ) then sum_penalties gs name
  else
    let current_turn_loss = float_of_int (sum_penalties gs name) in
    let gs2 = fst (move_player ((1,1), gs) false) in
    let gs3 = fst (move_player ((1,2), gs) false) in
    let gs4 = fst (move_player ((1,3), gs) false) in
    let gs5 = fst (move_player ((1,4), gs) false) in
    let gs6 = fst (move_player ((1,5), gs) false) in
    let gs7 = fst (move_player ((1,6), gs) false) in
    let gs8 = fst (move_player ((6,2), gs) false) in
    let gs9 = fst (move_player ((6,3), gs) false) in
    let gs10 = fst (move_player ((6,4), gs) false) in
    let gs11 = fst (move_player ((6,5), gs) false) in
    let gs12 = fst (move_player ((6,6), gs) false) in

    let next_turn_loss =
    (1./.36.)*.(float_of_int (expected_loss_base gs2 name (i-1)) +.
                float_of_int (expected_loss_base gs12 name (i-1)))+.
    (2./.36.)*.(float_of_int (expected_loss_base gs3 name (i-1)) +.
                float_of_int (expected_loss_base gs11 name (i-1)))+.
    (3./.36.)*.(float_of_int (expected_loss_base gs4 name (i-1)) +.
                float_of_int (expected_loss_base gs10 name (i-1)))+.
    (4./.36.)*.(float_of_int (expected_loss_base gs5 name (i-1)) +.
                float_of_int (expected_loss_base gs9 name (i-1)))+.
    (5./.36.)*.(float_of_int (expected_loss_base gs6 name (i-1)) +.
                float_of_int (expected_loss_base gs8 name (i-1)))+.
    (6./.36.)*.(float_of_int (expected_loss_base gs7 name (i-1))) in


    let return_float =
      (current_turn_loss +. (float_of_int (i-1)) *. next_turn_loss) /. (float_of_int i) in
    int_of_float return_float

(*##############helpers for calculating gain on properties####################*)

(*given an owner with name name, and a property, determine if they
profited from someone landing on it*)
let property_gain (gs: gamestate) (name: string) (data:prop_data) : int =
  match data.status with
  | Unowned -> 0
  | Owned(owner) -> if(owner <> name) then 0
                    else (List.nth data.rents 0 )
  | Monopoly(owner) -> if(owner <> name) then 0
                       else 2*(List.nth data.rents 0 )
  | Houses(owner,num_houses) -> if(owner <> name) then 0
                                else (List.nth data.rents num_houses)
  | Hotel(owner) -> if(owner <> name) then 0 else (List.nth data.rents 5 )
  | Mortgaged(_)-> 0
  | Railroads(owner, num_rr)-> if(owner <> name) then 0
                               else (List.nth data.rents (num_rr -1) )

  | Utilities(owner, num_util)-> if(owner <> name) then 0
                                 else 7*(List.nth data.rents (num_util -1))

(* given an owner
the name of another player
loc of that player
determine how much they player owes the owner*)
let gain_on_space (gs: gamestate) (loc: int) (owner:string) (name: string) : int =
  match List.nth gs.spaces (loc mod 40) with
  | Property(p) -> property_gain gs owner p
  | Go -> 0
  | Jail(_) -> 0 (* list records # of turns player has been
                                   stuck in jail *)
  | Gotojail -> 0
  | Freeparking -> 0
  | Chance -> 0
  | Communitychest -> 0(* Added up money from cards you lose on, divided by
                            number of cards in the deck. *)
  | Utility(p) -> property_gain gs owner p
  | Railroad(p) -> property_gain gs owner p
  | Luxurytax i -> i
  | Incometax i -> i

(*name is the name of individual paying
owner owns property *)
let sum_indv_gains (gs:gamestate) (name:string) (owner:string)=
  let loc = (unwrap_player(get_player name gs)).location in
  let result =
  (1./.36.)*.((float_of_int (gain_on_space gs (loc+2) owner name) )+.
             (float_of_int (gain_on_space gs (loc+12) owner name) ))+.
  (2./.36.)*.((float_of_int (gain_on_space gs (loc+3) owner name) )+.
             (float_of_int (gain_on_space gs (loc+11) owner name) ))+.
  (3./.36.)*.((float_of_int (gain_on_space gs (loc+4) owner name) )+.
             (float_of_int (gain_on_space gs (loc+10) owner name) ))+.
  (4./.36.)*.((float_of_int (gain_on_space gs (loc+5) owner name) )+.
             (float_of_int (gain_on_space gs (loc+9) owner name) ))+.
  (5./.36.)*.((float_of_int (gain_on_space gs (loc+6) owner name) )+.
             (float_of_int (gain_on_space gs (loc+8) owner name) ))+.
  (6./.36.)*.(float_of_int (gain_on_space gs (loc+7) owner name) ) in
  (int_of_float result)


(*calculates an estimate of how much the player will win in the future by
 * averaging losses accross future gamestates*)
let rec expected_indv_gain (gs:gamestate) (name:string) (owner:string) (i:int): int =
  if( i <= 1 ) then sum_indv_gains gs name owner
  else
    let current_turn_gain = float_of_int (sum_indv_gains gs name owner) in
    let gs2 = fst (move_player ((1,1), gs) false) in
    let gs3 = fst (move_player ((1,2), gs) false) in
    let gs4 = fst (move_player ((1,3), gs) false) in
    let gs5 = fst (move_player ((1,4), gs) false) in
    let gs6 = fst (move_player ((1,5), gs) false) in
    let gs7 = fst (move_player ((1,6), gs) false) in
    let gs8 = fst (move_player ((6,2), gs) false) in
    let gs9 = fst (move_player ((6,3), gs) false) in
    let gs10 = fst (move_player ((6,4), gs) false) in
    let gs11 = fst (move_player ((6,5), gs) false) in
    let gs12 = fst (move_player ((6,6), gs) false) in
    let next_turn_gain =
    (1./.36.)*.(float_of_int (expected_indv_gain gs2 name owner (i-1)) +.
                float_of_int (expected_indv_gain gs12 name owner (i-1)))+.
    (2./.36.)*.(float_of_int (expected_indv_gain gs3 name owner (i-1)) +.
                float_of_int (expected_indv_gain gs11 name owner (i-1)))+.
    (3./.36.)*.(float_of_int (expected_indv_gain gs4 name owner (i-1)) +.
                float_of_int (expected_indv_gain gs10 name owner (i-1)))+.
    (4./.36.)*.(float_of_int (expected_indv_gain gs5 name owner (i-1)) +.
                float_of_int (expected_indv_gain gs9 name owner (i-1)))+.
    (5./.36.)*.(float_of_int (expected_indv_gain gs6 name owner (i-1)) +.
                float_of_int (expected_indv_gain gs8 name owner (i-1)))+.
    (6./.36.)*.(float_of_int (expected_indv_gain gs7 name owner (i-1))) in

    let return_float =
      (current_turn_gain +. (float_of_int (i-1)) *. next_turn_gain) /. (float_of_int i) in
    let return_int = int_of_float return_float in
    return_int


(*###########################expected gain####################################*)

(* given a board, return the expected gain for that player

i.e. calculate the penalties that will be paid to you, multiply
by 3, divide by 40

eventually make it based on board location as well*)
let expected_gain (gs:gamestate) (name:string) (i:int): int =

  let rec helper = function
  |h::t -> if(name = (unwrap_player h).name) then 0 + helper t
    else expected_indv_gain gs ((unwrap_player h).name) (name) (i) + helper t
  |[] -> 0 in

  helper gs.players


(*############trying to anticipate other players actions######################*)

(* calculate expected losses for each turn given state of the board
i.e. multiply 1/40 times the penalty for landing on each space and
sum them

potentially make this dependent on current location to make it more
accurate

need to determine penalty for landing on chance, cc. *)
let expected_loss (gs:gamestate) (name:string) : int =
failwith "todo"


(*############################################################################*)

(* Given a list of gamestates, max_gs determines which gamestate has the
 * the greatest expected return in the future. *)
let rec max_gs (gs_list: gamestate list) (current_best: gamestate) (name:string): gamestate =
  match gs_list with
  | [] -> current_best
  | h::t ->
  let h_expected_gain = expected_gain h name 4 in
  let current_best_expected_gain = expected_gain current_best name 4 in
  if (h_expected_gain) > (current_best_expected_gain) then
    max_gs t h name
  else
    max_gs t current_best name

(*############maximize gain for building######################################*)

let list_index m l =
  let rec helper m l i =
    match l with
    |h::t -> if (m = h) then i else helper m t (i+1)
    |[] -> failwith "elment was not here" in
  helper m l 0

(*given a board maximize the expected pay-off from your spaces
by building houses and hotels

leave enough to compensate for that turns expected losses*)
let rec buy_houses (gs:gamestate) (name:string) (saving:int) (print: bool): gamestate =
(*am't of money i have to spend*)
(*figure if the loop continue i.e. money >= savings*)
(*then build list of houses. if empty end*)
(*then go through each of the properties, return a new game state for
each property where a house was bought*)
(*figure out which build gives me the highest expected gain*)
(*repeat the process until i can't build any more then repeat the process
until one of the first 2 base cases is not met*)

  let current_player = get_player name gs in
  let current_money = (unwrap_player current_player).money in
  let current_money' = current_money - saving in

  let helper (prop_name: string) : bool =
    let prop = get_property prop_name gs in
    let prop_data = unwrap_property prop in
    let correct_status = (match prop_data.status with
      |Monopoly(_) -> true
      |Houses(_) -> true
      |_-> false ) in
    (prop_data.houseprice <= current_money') && correct_status in


  let current_properties = (unwrap_player current_player).props in
  let house_properties = List.filter helper current_properties in

  if (house_properties = []) then gs

  else
    let new_gs_list =
      List.map (fun x -> process_build 1 ((get_property x gs), gs) false) house_properties in
    let new_max_gs = max_gs new_gs_list gs name in
    if(new_max_gs <> gs) then
      if print then
        let gs_index = list_index new_max_gs new_gs_list in
        let prop_name = List.nth house_properties gs_index in
        Printf.printf "\n%s bought a house on %s.\n" name prop_name ;
        buy_houses new_max_gs name saving print
      else
        buy_houses new_max_gs name saving print
    else
      gs


(* Determines whether or not the player can buy this property. *)
let valid_buy (pl:player) (data:prop_data) (gs:gamestate) =
  data.purchaseprice <= (unwrap_player pl).money && (data.status = Unowned)

(* Sees if the current space is a property. If it is, see if the player can
 * buy it and do so if it can. *)
let buy_prop (name:string) (gs: gamestate) (print: bool): gamestate =
    let pl = (get_player name gs) in
    let loc = (unwrap_player pl).location in
    let sp  = List.nth  gs.spaces loc in
    match sp with
    | Property p
    | Utility p
    | Railroad p-> if (valid_buy pl p gs) then
                       if print
                       then
                         (Printf.printf "\n%s has bought %s for $%i.\n" name p.title p.purchaseprice ;
                         process_prop_purchase pl sp gs)
                       else
                         process_prop_purchase pl sp gs
                     else
                       gs
    | _ -> gs

(*A special max GS for unmortgaging that takes into account building houses on
the unmortgaged property*)
let rec max_gs_unmortgage (gs_list: gamestate list) (current_best: gamestate) (current_best_int:int) (name:string) (savings:int): gamestate =
  match gs_list with
  | [] -> current_best
  | h::t ->
  let h_expected_gain = expected_gain (buy_houses h name savings false) name 4 in
  if (h_expected_gain) > (current_best_int) then
    max_gs_unmortgage t h h_expected_gain name savings
  else
    max_gs_unmortgage t current_best current_best_int name savings

(* Similar to buy_houses but for determines mortgaged properties to unmortgage. *)
let rec unmortgage (gs:gamestate) (name:string) (saving:int) (print: bool): gamestate =
  let current_player = get_player name gs in
  let current_money = (unwrap_player current_player).money in
  let current_money' = current_money - saving in

  let helper (prop_name: string) : bool =
    let prop = get_property prop_name gs in
    let prop_data = unwrap_property prop in
    let correct_status = (match prop_data.status with
      |Mortgaged(_) -> true
      |_-> false ) in
    ((prop_data.purchaseprice/2) + (prop_data.purchaseprice/10) <= current_money') &&
    correct_status in

  let current_properties = (unwrap_player current_player).props in
  let mortgaged_properties = List.filter helper current_properties in

  if (mortgaged_properties = []) then gs
  else
    let new_gs_list =
      List.map (fun x -> process_unmortgage ((get_property x gs), gs) false) mortgaged_properties in
    let seed_int = expected_gain gs name 1 in
    let new_max_gs = max_gs_unmortgage new_gs_list gs seed_int name  saving in
    if(new_max_gs <> gs) then
      if print then
        let gs_index = list_index new_max_gs new_gs_list in
        let prop_name = List.nth mortgaged_properties gs_index in
        let () = print_endline (name ^ " unmortgaged " ^ prop_name ^ ".\n") in
        unmortgage new_max_gs name saving print
      else
        unmortgage new_max_gs name saving print
    else
      gs


(* Returns the gamestate that maximizes the players expected future profits. *)
let max_gain (gs:gamestate) (name:string) (print: bool) : gamestate =
  let losses = expected_loss_base gs name 4 in
  let buy_prop_gs = buy_prop name gs print in
  let unmortgage_prop_gs = unmortgage buy_prop_gs name losses print in
  let buy_houses = buy_houses unmortgage_prop_gs name (losses) print in
  buy_houses



(*############minimize loss if negative#######################################*)

(*if the player has negative money,
  determine which properties to mortgage.

  The combination that would leave
  the player with the highest expected value *)
let rec min_losses (gs:gamestate) (name:string) : gamestate =

  let houses_helper (prop_name: string) : bool =
    let prop = get_property prop_name gs in
    let prop_data = unwrap_property prop in
    match prop_data.status with
      | Hotel(_) -> true
      | Houses(_) -> true
      | _-> false in

  let rec check_no_houses (gs: gamestate) (prop_names: string list): bool =
    match prop_names with
    | [] -> true
    | h::t -> let h_property_data = unwrap_property(get_property h gs) in
              match h_property_data.status with
              | Monopoly (_) -> true && (check_no_houses gs t)
              | Mortgaged (_) -> true && (check_no_houses gs t)
              | _ -> false in

let mortgage_helper (title : string) : bool =
  let prop_data = unwrap_property (get_property title gs) in
  match prop_data.status with
  | Unowned -> failwith "you don't own this"
  | Owned(_) -> true
  | Monopoly(owner)->
    let owner_data = unwrap_player(get_player owner gs) in
    let owner_props = owner_data.props in
    let owner_props_in_monopoly =
      List.filter (fun x -> ((unwrap_property(get_property x gs)).color)=prop_data.color) owner_props in
    check_no_houses gs owner_props_in_monopoly
  | Houses (_) -> false
  | Hotel (_) -> false
  | Mortgaged(_)-> false
  | Railroads(_) -> true
  | Utilities(_) -> true in

  let current_properties = (unwrap_player (get_player name gs)).props in
  let bulldoze_properties = List.filter houses_helper current_properties in
  let mortgage_properties = List.filter mortgage_helper current_properties in

  if ((unwrap_player (get_player name gs)).money > 0) then
  (*let () = print_endline "we have positive money again!" in*)
  gs
  (*else if check_liquidity name gs <= 0 then gs*)
  else if (bulldoze_properties = []) && (mortgage_properties = []) then
  let () = print_endline "No more houses or properties to mortage.\n" in
    gs
  else
    let new_gs_bulldoze =
      List.map (fun x -> process_bulldoze 1 ((get_property x gs), gs) false) bulldoze_properties in
    let new_gs_properties =
      List.map (fun x -> process_mortgage ((get_property x gs), gs) false ) mortgage_properties in
    let new_gs_list = new_gs_properties@new_gs_bulldoze in
    let new_gs = max_gs (List.tl new_gs_list) (List.hd new_gs_list) name in
    if (List.mem new_gs new_gs_bulldoze) then
        let gs_index = list_index new_gs new_gs_list in
        let prop_name = List.nth (mortgage_properties@bulldoze_properties) gs_index in
        let () = print_endline (name ^" bulldozed a house on " ^ prop_name ^ ".\n") in
      min_losses new_gs name
    else
      let gs_index = list_index new_gs new_gs_list in
      let prop_name = List.nth (mortgage_properties@bulldoze_properties) gs_index in
      let () = print_endline (name ^" mortgaged " ^ prop_name ^ ".\n") in
      min_losses new_gs name

(*############################mli files#######################################*)


(* Keeps track of how many turns in a row a player has rolled doubles. *)
let tridoubles = ref 0

(*prompts ai for pre-roll choice
then executes*)
let rec pre_roll (gs: gamestate): (gamestate) =
  let player = List.hd gs.players in
  let player_data = unwrap_player player in
  let player_name = player_data.name in
  let die = dice_roll () in
  let in_jail = is_in_jail player gs in
  let realdoubles = ((fst die) = (snd die) && not in_jail && (not (is_going_to_jail (fst die, snd die) player gs))) in

  if (in_jail) then
    let g' =
                if (get_prisoner player gs |> snd) = 2
                then
                  (if (fst die) = (snd die)
                   then release_from_jail player gs
                   else let newplayer = update_money player (-50) in
                        release_from_jail newplayer (update_player newplayer gs))
                else gs in
    change_board realdoubles (die, g') true


  else
    let min_gs = min_losses gs (find_self gs) in
    let () = print_endline ("\n" ^ player_name ^ " is thinking.") in
    let max_gs = max_gain min_gs (find_self min_gs) true in

    let new_game = change_board realdoubles (die, max_gs) true in
    if realdoubles && !tridoubles = 2 then
      (pmessage  ((unwrap_player player).name ^
                   " rolled three doubles in a row! You go to Jail!\n");
       gotojail_helper gs)
    else if realdoubles && !tridoubles < 2 then
      (incr tridoubles; pre_roll new_game)
    else new_game


(*prompts ai for post-roll choice
then executes*)
let rec post_roll (gs: gamestate): (gamestate) =
  (* For now AI won't offer trades or decide to
  * build on/mortgage properites. Just immediately
  * return the current gamestate [gs]. *)
  tridoubles := 0;

  let player = List.hd gs.players in
  let player_data = unwrap_player player in
  let player_name = player_data.name in

  if (is_in_jail player gs) then
    let min_gs = min_losses gs (find_self gs) in
    Printf.printf "\n%s has ended their turn.\n" (unwrap_player player).name ;
    min_gs
  else
    let min_gs = min_losses gs (find_self gs) in
    let () = print_endline ("\n" ^ player_name ^ " is thinking.") in
    let max_gs = max_gain min_gs (find_self min_gs) true in
    Printf.printf "\n%s has ended their turn.\n" (unwrap_player player).name ;
    max_gs



(*prompts ai for trade choice then executes*)
let respond_to_trade (tr, gs) : bool =
  let name = (unwrap_player tr.request.tplayer).name in
  let () = print_endline ("Computer is evaluating trade...\n") in
  let trade_gs = max_gain (process_trade tr gs false) name false in
  let trade_gain = expected_gain trade_gs name 4 in
  let trade_loss = expected_loss_base trade_gs name 4 in
  let gain = expected_gain (max_gain gs name false) name 4 in
  let loss = expected_loss_base (max_gain gs name false) name 4 in

  let trade_offer_money = (tr.offer).tmoney in
  let trade_request_money = (tr.request).tmoney in
  (trade_gain - trade_loss + (trade_offer_money/20) - (trade_request_money/10)) > (gain - loss)