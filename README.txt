To play our game compile using:

cs3110 compile main.ml
cs3110 run main.ml

To use the testboard type

cs3110 compile main_test.ml
cs3110 run main_test.ml

Please note:

**When using the testboard, the AI may take a while to make its' turn as it needs
to adjust to the new state of the board. This is normal behavior.

**Monopoly has some convoluted rules. If something seems incorrect, it is probably
because the actual rules call for it. The only rule we are missing is auctions

**A new game started with 4 AI will take a REALLY long time to end because AI
players will not trade with eachother. As a result, monopolies will not be 
allocated. To see a game to completion choose 4 AI in test_main with randomly
distributed monopolies.