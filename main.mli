open Gamestate
open Human_turn
open Ai_turn

(** initialize queries the user to provide additional setup information
    (i.e. the number of human players, number of ai players ) *)
val initialize: gamestate -> gamestate

(** Checks if the first player in the input gamestate has negative money.
    If so, a gamestate is produced with this player removed and a defeat
    message is printed. If only a single player then remains, a
    congratulations message is printed and the program exits. If the first
    player has nonnegative money, then a new gamestate is produced with the
    first player cycled to the back of the player list *)
val check_defeat: gamestate -> gamestate

(** Recursive function that represents a single turn of monopoly.
    Will decide if current player is human or ai, then call pre_roll
    and post_roll appropriately. Will check defeat and either end
    the game or call itself with the next player at the front of
    the playerlist in gamestate. *)
val gameloop: gamestate -> gamestate

(** This function begins a game of monopoly. It will call run
    initialize first, then gameloop *)
val main: unit -> unit