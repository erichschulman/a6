open Gamestate

(** This module calculate the decisions made by the ai players. *)

(** Pre_roll calculates the decisions of an ai player before they roll
    on their turn. This includes offering trades, building/mortgaging
    properties, and rolling the die to end this phase. *)
val pre_roll: gamestate -> gamestate

(** Post_roll calculates the decisions of an ai player after they
    roll and land on a new space. This includes offering trades and
    building/mortgaging properties. *)
val post_roll: gamestate -> gamestate

(** Precondition: trade.request.owner is an AI player.
    Respond_to_trade offers the ai player the given trade and calculates
    and prints its response. After accepting or rejecting, the new
    gamestate will be produced. *)
val respond_to_trade:  trade * gamestate -> bool
