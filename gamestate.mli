(** Abstraction function: The current status of a game of monopoly is
    represented by a value of type [gamestate]. Changes in the game
    produce new values of type [gamestate]. A value of type [gamestate]
    will use values of the types [player], [space], [prop_data], and
   [card] to organize game information *)

type gamestate = {
    players : player list ; (* the list order gives the turn order *)
    chance : card list ; (* list of chance cards *)
    cc : card list ; (* list of community chest cards *)
    spaces : space list (* order of spaces represents order on board *)
  }

and player =
  | Human of playerdata
  | Ai of playerdata

and playerdata = {
    money : int ;
    location : int ; (* 0 = Go, 39 = Boardwalk *)
    props : string list ; (* Names of properties owned *)
    name : string ;
    jailcard : bool
  }

and space =
  | Property of prop_data
  | Go
  | Jail of (string * int) list (* list records # of turns player has been
                                   stuck in jail *)
  | Gotojail
  | Freeparking
  | Chance
  | Communitychest
  | Utility of prop_data
  | Railroad of prop_data
  | Luxurytax of int
  | Incometax of int

and prop_data = {
    title : string ;
    purchaseprice : int ;
    houseprice : int ;
    rents : int list ; (* records rents corresponding to # of houses *)
    status : propstatus ;
    color : string }

and propstatus =
  | Unowned
  | Owned of string (* string = name of player who owns it*)
  | Monopoly of string (* string owns all other properties of this color *)
  | Houses of string * int (* int between 1 and 4 is the number of houses *)
  | Hotel of string
  | Mortgaged of string
  | Railroads of string * int (* int between 1 and 4 is the # of RR's owned *)
  | Utilities of string * int (* int between 1 and 2 is # of Utilities owned *)

and card =
  | Move of int * string (* int represents location the card moves you
                          to, with 0 = Go and 39 = Boardwalk *)
  | Pay of int * string (* string represents card text *)
  | Payall of int * string (* pay each other player int dollars *)
  | Collectfromall of int*string (* each other player pays you int dollars *)
  | Gotojailcard of string
  | Getoutofjail of string
  | Nearestutility of string
  | Nearestrailroad of string
  | Goback3spaces of string
  | Payrepairs of int*int*string (* (x,y) = pay x per house and y per hotel *)

(** Offer includes the properties and money being offered in the trade
    and request includes the properties and money being requested *)
and trade = {
    offer : trade_bundle ;
    request : trade_bundle
  }

and trade_bundle = {
    tplayer : player ;
    tmoney : int ;
    tproperties : string list
  }

(** Prints a list of the current player's assets *)
val print_currentplayer_assets: gamestate -> unit

(** Prints a list of all players' current assets *)
val print_all_assets: gamestate -> unit

(** find_space [string] [gamestate] returns the space with title [string]
    within [gamestate], or None if no such space exists.*)
val find_space: string -> gamestate -> space option

(** returns the location of the input space in the gamestate's space list *)
val prop_location: string -> gamestate -> int

(** returns the player with name [string] within [gamestate], or None if
    no player's name matches [string] *)
val find_player: string -> gamestate -> player option

(** spacename n g returns the name of the space at location n in gamestate g *)
val spacename : int -> gamestate -> string

(** compares the location of the properties represented by the two strings
    and returns -1, 0, or 1.
    Useable to sort string lists representing owned properties *)
val compare_props : gamestate -> string -> string -> int

(** update_property [space] [gamestate] with find [space']  whose name
    matches the name of [space] in [gamestate] and replace [space']
    with [space].
    Precondition: [space] is either a Property, Utility, or Railroad*)
val update_property: space -> gamestate -> gamestate

(** update_jail [newlist] [gamestate] outputs a new gamestate where the data
    on the space Jail is [newlist] *)
val update_jail : (string * int) list -> gamestate -> gamestate

(** update_player [player] [gamestate] with find [player']  whose name
    matches the name of [player] in [gamestate] and replace [player']
    with [player]. *)
val update_player: player -> gamestate -> gamestate

(** check_liquidity [s] [g] returns the amount of money player [s] would have
    after selling all houses/hotels and mortgaging all properties. Returns
    None if [s] is not a player in [g] *)
val check_liquidity: string -> gamestate -> int

(** [initial_state] is a gamestate with the initial set up of the board. *)
val initial_state: gamestate
