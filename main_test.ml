open Gamestate
open Human_turn
open Ai_turn
open Turn_out
open Gameboard

let _ = Random.self_init ()


let filter_props spaces = List.filter is_property spaces

let cycle = function
   | [] -> []
   | a::b -> b @ [a]

let pieces =
   ["Battleship";"Cat";"Dog";"Hat";"Racecar";"Shoe";"Thimble";"Wheelbarrow"]

let tokens = ref pieces


(* prompt user to choose player types *)
let rec setup_players i n (g: gamestate) =
   match n with
   | 0 -> g
   | _ -> Printf.printf "\nIs Player %i a Human or an AI?\n> " i ;
            let pt = getinput() in
            match pt with
              | "ai" | "human" -> setup_players (i+1) (n-1) (add_player pt i g)
              | _ ->  pmessage "Invalid input. Try again."; setup_players i n g

(* prompts user to choose player gamepieces (tokens), then adds the players
   to the player list *)
and add_player t i g =
  Printf.printf "\nWhat should Player %i's token be? The choices are:\n%s.\n> "
                i (slist_to_string !tokens) ;
    let pn = read_line () in
        if (List.mem (pformat pn) !tokens) then
            if t = "ai" then
               (tokens := (remove_from_list (pformat pn) !tokens);
                {players = List.append g.players [Ai {name = pformat pn;
                  money = 1500; location = 0; props = []; jailcard = false}];
                 chance = g.chance;
                 cc = g.cc;
                 spaces = g.spaces}
               )
            else
                (tokens := (remove_from_list (pformat pn) !tokens);
                 {players = List.append g.players [Human {name = pformat pn;
                    money = 1500; location = 0; props = []; jailcard = false}];
                  chance = g.chance;
                  cc = g.cc;
                  spaces = g.spaces}
                )
        else (pmessage "Invalid token. Please try again."; add_player t i g)

let assign_prop property player gs =
      let playername = get_player_name player in
      let propname = get_prop_title property in
      let gs' = update_ownership gs playername propname in
      gs'

let rec assign_props cycling players spaces gs =
     match spaces with
     | [] -> gs
     | z::[] -> assign_prop z (List.nth players 0) gs
     | a::b -> assign_props cycling
               (if cycling then (cycle players) else players)
                b (assign_prop a (List.nth players 0) gs)

(* initialize a board for testing. If [randprops] is true then the board
is initialized with all the properties randomly assigned, if [randmonops]
is true then *)
let test_initialize g randprops randmonops =

let rec init_basic gs =
   pmessage "Please select number of players: (2-4)";
   let p = getinput() in
      match p with
      | "2" | "3" | "4" -> let pn = int_of_string p in
                           let rec init_players game n =
                           match n with
                           | 0 -> g
                           | i -> setup_players 1 i g
                           in init_players g pn

      | _ -> pmessage "Invalid number of players. Please try again.";
             init_basic g
  in
  let baseinit = init_basic g in
  if randprops then

  let shuffled_props = baseinit.spaces |> filter_props |> shuffle_list in

   let newinit = assign_props true baseinit.players shuffled_props baseinit in
   update_game_monopolies newinit true

  else if randmonops then

  let colors = List.fold_left (fun acc x ->

                  if is_property x && (not (List.mem (get_prop_color x) acc))
                  then (get_prop_color x)::acc else acc) [] baseinit.spaces
  in
  let csets = List.fold_left (fun acc x ->
             (get_color_set x baseinit.spaces)::acc) [] colors
  in
  let rec assign_monopolies players (csets:space list list) (gs:gamestate) =
  match csets with
  | [] -> gs
  | z::[] -> assign_props false players z gs
  | a::b -> assign_monopolies (cycle players) b (assign_props false players a gs)
  in
  let newinit' = assign_monopolies baseinit.players csets baseinit
  in
  let ngame = update_game_monopolies newinit' true in

  let (builtprops:gamestate)=
  List.fold_left
     (fun acc x -> if is_property x &&
     (match get_prop_status x with
      | Monopoly owner -> true
      | _ -> false ) then (process_build ((Random.int 5)+1) (x,acc) (true))
        else acc) ngame ngame.spaces in
  let addedmoney =
  List.fold_left (fun acc x -> update_player (update_money x 5000) acc)
                  builtprops builtprops.players
  in addedmoney

  else baseinit

(* create a string with the list of player types and gamepieces *)
let rec string_of_players plist n =
   match plist with
   | [] -> ""
   | z::[] -> (match z with
      | Ai x -> "Player " ^ (string_of_int n) ^ " AI: " ^ x.name ^ ".\n"
      | Human y -> "Player " ^ (string_of_int n) ^ " Human: " ^ y.name ^ ".\n"
              )
   | a::b -> (match a with
      | Ai x -> "Player " ^ (string_of_int n) ^ " AI: " ^ x.name ^ ", " ^
                 string_of_players b (n+1)
      | Human y -> "Player " ^ (string_of_int n) ^ " Human: " ^ y.name ^ ", " ^
                 string_of_players b (n+1)
              )


let check_defeat (g : gamestate) : gamestate =
  let (currentplayer, restofplayers) = match g.players with
      | [] -> failwith "No players in game"
      | h::t -> (h,t) in
  let currentplayerdata = match currentplayer with
      | Human d
      | Ai d -> d in
  if currentplayerdata.money >= 0
  then {g with players = (restofplayers @ [currentplayer]) }
  else let g' = process_defeat currentplayer g in
         (Printf.printf
         "%s has lost and been removed from the game.\n" currentplayerdata.name ;
        match restofplayers with
          | [] -> failwith "There should have been more than one player."
          | [Human winner]
          | [Ai winner] ->
             Printf.printf "Congratulations %s, you have won!\n" winner.name ;
             exit 0
          | _ -> {g' with players = restofplayers} )



let rec gameloop game =
  match (List.nth game.players 0) with
   | Human p -> game |> boardprinter(Human_turn.pre_roll)
                     |> Human_turn.post_roll
                     |> check_defeat
                     |> gameloop

   | Ai    p -> game |> Ai_turn.pre_roll
                     |> Ai_turn.post_roll
                     |> check_defeat
                     |> gameloop


let init = initial_state

let rec main () =
  printboard initial_state;
  Printf.printf "\nHello!\nWelcome to CS3110 Monopoly! \n\n";
  pmessage "Random properties: R; Random monopolies + buildings: M";

  let newg =
  match getinput () with
  | "r" ->  test_initialize { players = [] ;
                          chance  = shuffle_list init.chance;
                          cc      = shuffle_list init.cc;
                          spaces  = init.spaces }
                          true false

  | "m" ->  test_initialize { players = [] ;
                          chance  = shuffle_list init.chance;
                          cc      = shuffle_list init.cc;
                          spaces  = init.spaces }
                          false true
  | _ -> main ()

  in print_newline();

    let rec launch () =
    Printf.printf "\nPlease confirm the following setup:\n" ;
    print_players newg.players 1 ;
    Printf.printf "Is this correct? Y to start game, N to rechoose.\n> " ;
    let decision = getinput () in
       match decision with
       | "y" -> gameloop newg
       | "n" -> tokens:= pieces; main ()
       | "quit" -> exit 0
       | _ -> pmessage "Invalid input. Please try again."; launch ()
       in launch ()




let _ = main ()
