open Gamestate
open Human_turn
open Ai_turn
open Turn_out
open Gameboard

let _ = Random.self_init ()

let pieces =
   ["Boot";"Cat";"Dog";"Hat";"Racecar";"Ship";"Thimble";"Wheelbarrow"]

let tokens = ref pieces



(* prompt user to choose player types *)
let rec setup_players i n (g: gamestate) =
   match n with
   | 0 -> g
   | _ -> Printf.printf "\nIs Player %i a Human or an AI?\n> " i ;
            let pt = getinput() in
            match pt with
              | "ai" | "human" -> setup_players (i+1) (n-1) (add_player pt i g)
              | _ ->  pmessage "Invalid input. Try again."; setup_players i n g

(* prompts user to choose player gamepieces (tokens), then adds the players
   to the player list *)
and add_player t i g =
  Printf.printf "\nWhat should Player %i's token be? The choices are:\n%s.\n> "
                i (slist_to_string !tokens) ;
    let pn = read_line () in
        if (List.mem (pformat pn) !tokens) then
            if t = "ai" then
               (tokens := (remove_from_list (pformat pn) !tokens);
                {players = List.append g.players [Ai {name = pformat pn;
                  money = 1500; location = 0; props = []; jailcard = false}];
                 chance = g.chance;
                 cc = g.cc;
                 spaces = g.spaces}
               )
            else
                (tokens := (remove_from_list (pformat pn) !tokens);
                 {players = List.append g.players [Human {name = pformat pn;
                    money = 1500; location = 0; props = []; jailcard = false}];
                  chance = g.chance;
                  cc = g.cc;
                  spaces = g.spaces}
                )
        else (pmessage "Invalid token. Please try again."; add_player t i g)


let rec initialize g =
   Printf.printf "Please select the number of players: (2-4)\n> ";
   let p = getinput() in
      match p with
      | "2" | "3" | "4" -> let pn = int_of_string p in
                           let rec init_players game n =
                           match n with
                           | 0 -> g
                           | i -> setup_players 1 i g
                           in init_players g pn

      | _ -> pmessage "Invalid number of players. Please try again.";
             initialize g


let check_defeat (g : gamestate) : gamestate =
  let (currentplayer, restofplayers) = match g.players with
      | [] -> failwith "No players in game"
      | h::t -> (h,t) in
  let currentplayerdata = match currentplayer with
      | Human d
      | Ai d -> d in
  if currentplayerdata.money >= 0
  then {g with players = (restofplayers @ [currentplayer]) }
  else let g' = process_defeat currentplayer g in
         (Printf.printf
         "%s has lost and been removed from the game.\n" currentplayerdata.name ;
        match restofplayers with
          | [] -> failwith "There should have been more than one player."
          | [Human winner]
          | [Ai winner] ->
             Printf.printf "Congratulations %s, you have won!\n" winner.name ;
             exit 0
          | _ -> {g' with players = restofplayers} )


let rec gameloop game =
  match (List.nth game.players 0) with
   | Human p -> game |> boardprinter(Human_turn.pre_roll)
                     |> Human_turn.post_roll
                     |> check_defeat
                     |> gameloop

   | Ai    p -> game |> Ai_turn.pre_roll
                     |> Ai_turn.post_roll
                     |> check_defeat
                     |> gameloop


let init = initial_state

let rec main () =
  printboard initial_state;
  Printf.printf "\nHello!\nWelcome to CS3110 Monopoly! \n\n";
  let newg = initialize { players = [] ;
                          chance  = shuffle_list init.chance;
                          cc      = shuffle_list init.cc;
                          spaces  = init.spaces } in
  let rec launch () =
    Printf.printf "\nPlease confirm the following setup:\n" ;
    print_players newg.players 1 ;
    Printf.printf "Is this correct? Y to start game, N to rechoose.\n> " ;
    let decision = getinput () in
       match decision with
       | "y" -> let currentplayer = List.hd newg.players in
                let lastplayer = List.nth newg.players ((List.length newg.players) - 1) in
                (match (currentplayer,lastplayer) with
                 | (Human p1, Human p2) -> ()
                 | (Human p1, Ai p2) -> printboard newg
                 | (Ai p1, Human p2) -> printboard newg
                 | (Ai p1, Ai p2) -> printboard newg ) ;
                gameloop newg
       | "n" -> tokens:= pieces; main ()
       | "quit" -> exit 0
       | _ -> pmessage "Invalid input. Please try again."; launch ()
       in launch ()




let _ = main ()
