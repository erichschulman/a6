################################################################################
######################### missing gameplay #####################################
################################################################################

Auctions

################################################################################
######################### misc info ############################################
################################################################################


I added a main_test.ml file that is just like main.ml but initiates the board
with all the properties randomly assigned to all the players,
mostly for testing/debugging purposes. Try it out when you want to playtest!


to clear screen: Printf.printf "%c[2J [message]" (char_of_int 27);

Things we still need --> print functions for "View" input from player

Maybe it would be good to have the option to exit out of the mortgage/unmortgage
submenu and back to pre/post-roll loop if the player decides not to
mortgage/unmortgage anything (sort of like the QUIT in View).
Currently, there is no way to get out of that submenu unless you go ahead and
mortgage/unmortgage something. Same with trades.

Trades need to be accepted by the AI in human turn

You can only mortgage if all properties in the monopoly have 0 houses. We do not enforce
this in human.

Print need in trade repl for indicating incorrect amount specified

Process defeat and rolling 2x on doubles cause a bug with process defeat give the players
assets to the player who's space they're on. However, if you roll doubles and go negative, pre-roll is called again. As a result, you may not be on the space of the player who defeated you when pre-roll is called.


################################################################################
#########################known bugs#############################################
################################################################################

Bug Free Yay!

################################################################################
########################fixed bugs##############################################
################################################################################

Double print when the AI mortgages a property and losses a monopoly (I would imagine
this happens to humans as well.)

"Howdy Cat! You're currently on Kentucky Avenue (21/39) and have $5600.
Kentucky Avenue is owned by Kentucky Avenue who has built a hotel. Rent is $1050" uh?

jail...

when players, are in jail and roll doubles, they are removed and roll again (instead) of using the old roll

ai doesn't mortgage/sell properties correctly when it has negative money

print which property the ai is buying on and bulldozing on

mortgaging in human turn

ai prints too much

ai doesn't consider money when deciding on a trade

ai takes too long

Game allows a player to trade with himself

Players can choose to trade negative money

Bulldozing is not an option after building (probably not yet implemented?)

On the View menu, railroads (and probably utilities also) don't seem to
display the right rent cost. e.g. if I land on Reading Railroad and the other
player owns it (and only owns that one railroad), I get charged $25 as I should,
but the rent says $50 on the "view all" printout for some reason.

Trading doesn't actually transfer property from one player to another.

Status of railroad doesn't switch to Mortgaged after we mortgage it

print_build on human_turn is causing an exception after you build on a property
--> it might be trying to print the info from the old property
before the house was built, need to extract the property from the gamestate
that process_build returned and pass that to print_build.

When I buy a railroad it detects that I can mortgage something, but it doesn't
let me mortgage it when I go into the mortgage submenu. (perhaps an issue with
detecting what is available for mortgage which might not be adding
utilities/railroads to the list?)

Chance/cc cards decks might not be cycling after a card is drawn,
the same card stays at the top and is continually drawn by multiple players
(shuffling the decks now works correctly at the start of the game)

player being offered a trade doesn't get opportonunity to accept or reject
--> we think that in process_trade accept_trade might not be getting called for
the player being offered the trade. As a result, trades don't do anything
(the circular dependency issue needs to be addressed before this can be
fixed -Andres)

The View command doesn't seem to be working post-roll

don't get money for mortgaging a property => I did not notice this problem. I did
find a bug where unmortgaging properties was adding money instead of substracting it,
and it's fixed.

when we buy last space of one color, spaces aren't updated correctly i.e. they're
not considered monopolies --> somewhere in process_prop_purchase or one of the
helpers this function calls. => Fixed, was a bug in a helper of update_monopoly,
and message is now printed when a monopoly is acquired or lost.

don't shuffle community chest and chance cards --> probably somewhere in main, might
need to init random number generator??? => Yup, fixed.

sometimes income tax gives a postive am't instead of subtracting from the
players $ (yes, I noticed this too. -Andres)

Initial gamestate had utilities/railroads coded as Property spaces, should be
Utility/Railroad; was causing issues with monopoly updates, fixed

Buying property raises error, i.e. "Not a property", seems like the error is
raised in process_prop_purchase : turned out to be a problem in
updating monopolies, fixed

rolls may not be random

Pre-roll calls itself 2x in the game loop i.e. it repeats itself,
and it shouldn't

There seems to be an issue in the pre_roll loop, it works fine if you just
roll the dice and end your turn, but if you do anything like buying a property
or typing "view," then afterwards it lets you roll again even if you
had already rolled (and it was not a double). -Andres --> post_roll was calling
pre-roll instead recursively calling itself.

when trading, allows you to say yes to trading more properties even when
you don't have any more to trade