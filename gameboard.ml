open Gamestate



let ms' i g : string =
  let prop = List.nth g.spaces i in
  let pstatus = match prop with | Property data -> data.status | _ -> Unowned in
  let n = match pstatus with
                  | Houses (owner,hs) -> hs | Hotel owner -> 5 | _ -> 0 in
  let leftside = if n = 3 then " ⌂"
                 else if n = 4 then "⌂⌂"
                 else "  " in
  let rightside = if n = 1 then " ⌂"
                  else if n = 0 then "  "
                  else if n = 5 then " □"
                  else "⌂⌂"

  in
  let str =
  if i < 10 then leftside ^ string_of_int(i) ^ " " ^ rightside
  else leftside ^ string_of_int(i) ^ rightside
  in str





(** ms stands for make string.
 *  Creates the string representing the players on space [i] for the gamestate [g] *)
let ms (i : int) (g : gamestate) : string =
  let currentplayers : player list = List.filter
                                       (fun p ->
                                        match p with
                                        | Human data
                                        | Ai data -> data.location = i)
                                       g.players in
  let tokenlist : string list = List.map
                                  (fun p ->
                                    match p with
                                     | Human data
                                     | Ai data -> String.uppercase (String.sub data.name 0 1) )
                                  currentplayers in
  match List.length tokenlist with
    | 0 -> "______"
    | 1 -> "__" ^ (List.nth tokenlist 0) ^ "___"
    | 2 -> "_" ^ (List.nth tokenlist 0) ^ "_" ^ (List.nth tokenlist 1) ^ "__"
    | 3 -> (List.nth tokenlist 0) ^ "_" ^ (List.nth tokenlist 1) ^ "_"  ^ (List.nth tokenlist 2) ^ "_"
    | 4 -> (List.nth tokenlist 0) ^ "_" ^ (List.nth tokenlist 1) ^ "_"  ^ (List.nth tokenlist 2)
           ^ (List.nth tokenlist 3)
    | 5 -> (List.nth tokenlist 0) ^ "_" ^ (List.nth tokenlist 1) ^ (List.nth tokenlist 2) ^
             (List.nth tokenlist 3) ^ (List.nth tokenlist 4)
    | _ -> (List.nth tokenlist 0) ^ (List.nth tokenlist 1) ^ (List.nth tokenlist 2) ^
             (List.nth tokenlist 3) ^ (List.nth tokenlist 4) ^ (List.nth tokenlist 5)



let printboard (g : gamestate) : unit =
  Printf.printf " ____________________________________________________________________________ \n" ;
  Printf.printf "|Free P|Ken Av|Chance|Ind Av|Ill Av|B&O RR|Atl Av|Ven Av|Water |Marvin|GoToJa|\n" ;
  Printf.printf "|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|\n"
  (ms' 20 g) (ms' 21 g) (ms' 22 g) (ms' 23 g) (ms' 24 g) (ms' 25 g) (ms' 26 g) (ms' 27 g) (ms' 28 g) (ms' 29 g) (ms' 30 g);
  Printf.printf "|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|\n"
  (ms 20 g)  (ms 21 g)  (ms 22 g) (ms 23 g) (ms 24 g)  (ms 25 g)  (ms 26 g)  (ms 27 g)  (ms 28 g)  (ms 29 g)  (ms 30 g);
  Printf.printf "|NY Ave|                                                              |Pac Av|\n" ;
  Printf.printf     "|%s|                                                              |%s|\n" (ms' 19 g) (ms' 31 g);
  Printf.printf     "|%s|                                                              |%s|\n" (ms 19 g) (ms 31 g);
  Printf.printf "|TennAv|                                                              |NC Ave|\n" ;
  Printf.printf     "|%s|                                                              |%s|\n" (ms' 18 g) (ms' 32 g);
  Printf.printf     "|%s|                                                              |%s|\n" (ms 18 g) (ms 32 g) ;
  Printf.printf "|Chest |                                                              |Chest |\n" ;
  Printf.printf     "|%s|                ___   ___   ___   __   __   ___               |%s|\n" (ms' 17 g) (ms' 33 g);
  Printf.printf     "|%s|               /  _\\ /  _\\ /_  \\ /  | /  | /   \\              |%s|\n" (ms 17 g) (ms 33 g);
  Printf.printf "|St Jam|               | |    \\ \\    / /  | |  | | | | |              |PennAv|\n" ;
  Printf.printf     "|%s|               | |_   _\\ \\  _\\ \\  | |  | | | | |              |%s|\n" (ms' 16 g) (ms' 34 g);
  Printf.printf     "|%s|               \\___/ \\___/ \\___/  |_|  |_| \\___/              |%s|\n" (ms 16 g) (ms 34 g);
  Printf.printf "|PennRR|    __    __   ___   _   _   ___   ____   ___   _     _   _   |Short |\n" ;
  Printf.printf     "|%s|   |  \\  /  | /   \\ |  \\| | /   \\ | __ | /   \\ | |   | | | |  |%s|\n" (ms' 15 g) (ms' 35 g);
  Printf.printf     "|%s|   | \\ \\/ / | | | | | \\ \\ | | | | |  __| | | | | |   | |_| |  |%s|\n"(ms 15 g) (ms 35 g) ;
  Printf.printf "|Vir Av|   | |\\__/| | | | | | |\\ \\| | | | | |    | | | | |__  \\   /   |Chance|\n" ;
  Printf.printf     "|%s|   |_|    |_| \\___/ |_| \\_| \\___/ |_|    \\___/ |____|  |_|    |%s|\n" (ms' 14 g) (ms' 36 g);
  Printf.printf     "|%s|                                                              |%s|\n" (ms 14 g) (ms 36 g);
  Printf.printf "|Sta Av|                                                              |Park P|\n" ;
  Printf.printf     "|%s|                                                              |%s|\n" (ms' 13 g) (ms' 37 g);
  Printf.printf     "|%s|                                                              |%s|\n" (ms 13 g) (ms 37 g);
  Printf.printf "|Ele Co|                                                              |LuxTax|\n" ;
  Printf.printf     "|%s|                                                              |%s|\n" (ms' 12 g) (ms' 38 g);
  Printf.printf     "|%s|                                                              |%s|\n" (ms 12 g) (ms 38 g);
  Printf.printf "|St Cha|                                                              |Boardw|\n" ;
  Printf.printf     "|%s|                                                              |%s|\n" (ms' 11 g) (ms' 39 g);
  Printf.printf     "|%s|______________________________________________________________|%s|\n" (ms 11 g) (ms 39 g);
  Printf.printf "| Jail |Con Av|Ver Av|Chance|Ori Av|ReadRR|In Tax|Baltic|Chest |Med Av|  Go  |\n" ;
  Printf.printf "|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|\n"
  (ms' 10 g) (ms' 9 g) (ms' 8 g) (ms' 7 g) (ms' 6 g) (ms' 5 g) (ms' 4 g) (ms' 3 g) (ms' 2 g) (ms' 1 g) (ms' 0 g);
  Printf.printf "|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|\n"
  (ms 10 g) (ms 9 g) (ms 8 g) (ms 7 g) (ms 6 g) (ms 5 g) (ms 4 g) (ms 3 g) (ms 2 g) (ms 1 g) (ms 0 g)


let rec view (g : gamestate) : unit =
  Printf.printf "\nWhat would you like to view?\n" ;
  Printf.printf "Type BOARD to view the gameboard.\n" ;
  Printf.printf "Type ASSETS to view your current assets.\n" ;
  Printf.printf "Type ALL to view all players assets.\n" ;
  Printf.printf "Type N where N is an integer to get info on space N.\n" ;
  Printf.printf "Type QUIT to finish viewing.\n> " ;

  match (String.uppercase (read_line ()) ) with
    | "BOARD" -> printboard g ; view g
    | "ASSETS" -> print_currentplayer_assets g ; view g
    | "ALL" -> print_all_assets g ; view g
    | "QUIT" -> ()
    | s -> parsestring s g

and parsestring (s : string) (g : gamestate) : unit =
  try print_space_data (int_of_string s) g with
    | Failure "int_of_string"
    | Failure "nth" -> Printf.printf "Sorry, not a valid request.\n" ; view g
    | _ -> Printf.printf "Sorry, not a valid request.\n"

and print_jail_data (j : (string * int) list) (g : gamestate) : unit =
  Printf.printf "\nJail.\n" ;
  let rec helper = function
      | [] -> ()
      | h::t ->
         Printf.printf "%s has been stuck in jail for the past %i turns.\n"
                       (fst h) (snd h);
         helper t in
  helper j

and unowned_helper (s : space) : unit =
  match s with
    | Property d ->  Printf.printf "\n%s is %s and currently unowned.\n"
                                    d.title d.color ;
                      Printf.printf "The purchase price is $%i.\n"
                                    d.purchaseprice ;
    | Utility d -> Printf.printf "\n%s is a utility and currently unowned.\n"
                                    d.title ;
                      Printf.printf "The purchase price is $%i.\n"
                                    d.purchaseprice ;
    | Railroad d ->  Printf.printf "\n%s is a railroad and currently unowned.\n"
                                    d.title ;
                      Printf.printf "The purchase price is $%i.\n"
                                    d.purchaseprice ;
    | _ -> ()

(** prints information about the space at location n on the gameboard *)
and print_space_data (n : int) (g : gamestate) : unit =
  match List.nth g.spaces n with
    | Property d
    | Utility d
    | Railroad d ->
       (match d.status with
         | Unowned -> unowned_helper (List.nth g.spaces n)

         | Owned s -> Printf.printf "\n%s is %s and owned by %s.\n"
                                    d.title d.color s ;
                      Printf.printf "There are no houses built, and rent is $%i.\n"
                                   (List.hd d.rents)

         | Monopoly s -> Printf.printf "\n%s is %s and owned by %s.\n"
                                       d.title d.color s ;
                         Printf.printf "%s owns a monopoly on %s, so rent is $%i.\n"
                                       s d.color ((List.hd d.rents) * 2)

         | Houses (s,1) -> Printf.printf "\n%s is %s and owned by %s.\n"
                                         d.title d.color s ;
                           Printf.printf "1 house has been built so rent is $%i.\n"
                                        (List.nth d.rents 1)

         | Houses (s,n) -> Printf.printf "\n%s is %s and owned by %s.\n"
                                         d.title d.color s ;
                           Printf.printf "%i houses have been built so rent is $%i.\n"
                                         n (List.nth d.rents n)

         | Hotel s ->  Printf.printf "\n%s is %s and owned by %s.\n"
                                     d.title d.color s;
                       Printf.printf "A hotel has been built so rent is $%i.\n"
                                     (List.nth d.rents 5)

         | Mortgaged s ->  Printf.printf "\n%s is %s and owned by %s, but it has been mortgaged.\n"
                                         d.title d.color s

         | Railroads (s,n) -> Printf.printf "\n%s is a railroad owned by %s.\n"
                                            d.title s ;
                              Printf.printf "Since %s owns %i other railroads, rent is $%i\n"
                                            s (n-1) (List.nth d.rents (n-1))
         | Utilities (s,n) -> Printf.printf "\n%s is a utility owned by %s.\n"
                                            d.title s ;
                              Printf.printf "Since %s owns %i other utilities, rent is $(%i x dice roll).\n"
                                            s (n-1) (List.nth d.rents (n-1)) )
    | Go -> Printf.printf "\nGo - Collect $200 for passing.\n"
    | Jail j -> print_jail_data j g
    | Gotojail -> Printf.printf "\nGo directly to jail if you land here.\n" ;
                  Printf.printf "Do not pass Go, do not collect $200.\n"
    | Freeparking -> Printf.printf "\nFree Parking.\n"
    | Chance -> Printf.printf "\nDraw a Chance card.\n"
    | Communitychest -> Printf.printf "\nDraw a Community Chest card.\n"
    | Luxurytax x -> Printf.printf "\nPay a Luxury Tax of $%i.\n" x
    | Incometax x -> Printf.printf "\nPay Income Tax of $%i.\n" x

let rec print_players (plist : player list) (n:int): unit =
   match plist with
   | [] -> ()
   | (Human d)::tail -> Printf.printf "Player %i -- Human -- %s\n" n d.name ;
                print_players tail (n+1)
   | (Ai d)::tail -> Printf.printf "Player %i -- AI -- %s\n" n d.name ;
                print_players tail (n+1)

let boardprinter (f : gamestate -> gamestate) (g : gamestate) : gamestate =
  let currentplayer = List.hd g.players in
  let lastplayer = List.nth g.players ((List.length g.players) - 1) in
  (match (currentplayer,lastplayer) with
    | (Human p1, Human p2) -> printboard g
    | (Human p1, Ai p2) -> ()
    | (Ai p1, Human p2) -> ()
    | (Ai p1, Ai p2) -> () ) ;
  f g
